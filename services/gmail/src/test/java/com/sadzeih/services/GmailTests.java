package com.sadzeih.services;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import com.netflix.discovery.shared.Application;
import com.sadzeih.beans.GMailListener;
import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.utils.Status;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * Created by sadzeih on 2/8/17.
 */
@EnableAutoConfiguration
@EnableDiscoveryClient
@Import(GmailService.class)
class GmailServerTest {
    public static void main(String[] args) {
        System.setProperty("spring.config.name", "gmail-server");
        SpringApplication.run(GmailServerTest.class, args);
    }
}


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext
public class GmailTests {

    @Autowired
    private RestTemplate restTemplate;

    private final static String url = "http://gmail-service";

    public GmailTests()
    {
        GMailListener action = new GMailListener("ListenerTest", null, "LabelTest", "TopicTest");
        DAOFactory.getInstance().getGMailListenerDao().create(action, (long)1);
    }

    @Test
    public void testFindListener() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("userId", String.valueOf(1));
        map.add("id", String.valueOf(1));
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        Status status = this.restTemplate.postForObject(url+"/findListener", request, Status.class);
        GMailListener listener = (GMailListener)status.getObject();
        assertTrue(listener.getName().equals("ListenerTest"));
        assertTrue(listener.getLabelName().equals("LabelTest"));
        assertTrue(listener.getTopicName().equals("TopicTest"));
    }

    @Test
    public void testCreateListener() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("userId", String.valueOf(1));
        map.add("name", "createTestName");
        map.add("topicName", "createTestTopic");
        map.add("labelName", "createTestLabel");
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        this.restTemplate.postForObject(url+"/createListener", request, Status.class);
        GMailListener listener = DAOFactory.getInstance().getGMailListenerDao().find((long)2, (long)1);
        assertNotNull(listener);
    }
}
