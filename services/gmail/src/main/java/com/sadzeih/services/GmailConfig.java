package com.sadzeih.services;

import com.sadzeih.utils.GlobalVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;

/**
 * Created by devos-f on 10/01/17.
 */

@Configuration
@PropertySource("classpath:gmail.properties")
public class GmailConfig {

    @Autowired
    Environment env;

    @Bean
    public ConfigBean config()
    {
        ConfigBean config = new ConfigBean();
        config.setClientId(env.getProperty("auth.clientId"));
        config.setSecretId(env.getProperty("auth.secretId"));
        config.setRedirectURI(GlobalVariables.getAPIUrl() + "/gmail/callback");
        config.setAuthorizeURL(env.getProperty("auth.authorizeURL"));
        config.setAccessTokenURL(env.getProperty("auth.accessTokenURL"));

        return (config);
    }
}

