package com.sadzeih.services;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.*;
import com.google.api.services.gmail.model.Message;
import com.sadzeih.beans.*;
import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.utils.Status;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Created by sadzeih on 2/4/17.
 */
@Component
public class ListenersActions {

    @Autowired
    public RestTemplate restTemplate;

    private static final JsonFactory JsonFactory = JacksonFactory.getDefaultInstance();
    private static HttpTransport HttpTransport;

    static {
        try {
            HttpTransport = GoogleNetHttpTransport.newTrustedTransport();
        }
        catch (GeneralSecurityException se)
        {
            se.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public ListenersActions()
    {
        try {
            List<GMailListener> listeners = DAOFactory.getInstance().getGMailListenerDao().findAll();
            for (GMailListener listener: listeners) {
                AccessToken token = DAOFactory.getInstance().getAccessTokenDao().find(listener.getUserId(), "gmail");
                if (token != null) {
                    AccessToken refreshToken = DAOFactory.getInstance().getAccessTokenDao().find(listener.getUserId(), "gmailRefresh");
                    GmailAuth auth = new GmailAuth();
                    GoogleCredential credential = new GoogleCredential.Builder().setTransport(HttpTransport).setJsonFactory(JsonFactory).setClientSecrets(auth.getClientId(), auth.getSecretId()).build();
                    credential.setAccessToken(token.getToken());
                    credential.setRefreshToken(refreshToken.getToken());
                    Gmail service = getGmailService(credential);
                    fullSync(token.getUserId(), service);
                }
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private static Gmail getGmailService(GoogleCredential credential)
    {
        return new Gmail.Builder(HttpTransport, JsonFactory, credential).setApplicationName("Area").build();
    }

    static public void sendMail(GMailAction action) throws IOException, MessagingException
    {
        AccessToken token = DAOFactory.getInstance().getAccessTokenDao().find(action.getUserId(), "gmail");
        AccessToken refreshToken = DAOFactory.getInstance().getAccessTokenDao().find(action.getUserId(), "gmailRefresh");
        Users user = DAOFactory.getInstance().getUsersDao().findById(action.getUserId());
        GmailAuth auth = new GmailAuth();
        GoogleCredential credential = new GoogleCredential.Builder().setTransport(HttpTransport).setJsonFactory(JsonFactory).setClientSecrets(auth.getClientId(), auth.getSecretId()).build();
        credential.setAccessToken(token.getToken());
        credential.setRefreshToken(refreshToken.getToken());
        Gmail service = getGmailService(credential);
        Message message = new Message();
        Properties properties = System.getProperties();
        Session session = Session.getDefaultInstance(properties);
        MimeMessage email = new MimeMessage(session);
        email.setFrom(new InternetAddress(user.getEmail()));
        email.setSubject(action.getSubject());
        email.setRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(action.getDestinationAddress()));
        email.setText(action.getBody());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        email.writeTo(baos);
        String encodedEmail = Base64.encodeBase64URLSafeString(baos.toByteArray());
        message.setRaw(encodedEmail);
        service.users().messages().send("me", message).execute();
    }

    public void fullSync(Long userId, Gmail service) throws IOException
    {
        ListMessagesResponse response = service.users().messages().list("me").execute();
        List<Message> messages = new ArrayList<>();
        if (response.getMessages() != null) {
            messages.addAll(response.getMessages());
        }
        for (Message message: messages)
        {
            Message mes = service.users().messages().get("me", message.getId()).execute();
            HistoryId id = DAOFactory.getInstance().getHistoryIdDao().find(userId);
            if (id != null)
                DAOFactory.getInstance().getHistoryIdDao().remove(id.getId());
            id = new HistoryId();
            id.setHistoryId(mes.getHistoryId());
            id.setUserId(userId);
            DAOFactory.getInstance().getHistoryIdDao().create(id);
            break;
        }
    }

    @Scheduled(fixedRate = 5000)
    public void listening() throws IOException
    {
        List<GMailListener> listeners = DAOFactory.getInstance().getGMailListenerDao().findAll();
        for (GMailListener listener: listeners)
        {
            AccessToken token = DAOFactory.getInstance().getAccessTokenDao().find(listener.getUserId(), "gmail");
            AccessToken refreshToken = DAOFactory.getInstance().getAccessTokenDao().find(listener.getUserId(), "gmailRefresh");
            if (refreshToken != null) {
                GmailAuth auth = new GmailAuth();
                GoogleCredential credential = new GoogleCredential.Builder().setTransport(HttpTransport).setJsonFactory(JsonFactory).setClientSecrets(auth.getClientId(), auth.getSecretId()).build();
                credential.setAccessToken(token.getToken());
                credential.setRefreshToken(refreshToken.getToken());
                Gmail service = getGmailService(credential);
                if (listener.getType().equals("gmail"))
                {
                    HistoryId id = DAOFactory.getInstance().getHistoryIdDao().find(listener.getUserId());
                    if (id != null) {
                        ListMessagesResponse response = service.users().messages().list("me").execute();
                        List<Message> messages = new ArrayList<>();
                        if (response.getMessages() != null) {
                            messages.addAll(response.getMessages());
                        }
                        Message fullMessage = new Message();
                        for (Message message: messages)
                        {
                            fullMessage = service.users().messages().get("me", message.getId()).execute();
                            break;
                        }
                        if (!fullMessage.getHistoryId().equals(id.getHistoryId())) {
                            GMailListener fullListener = DAOFactory.getInstance().getGMailListenerDao().find(listener.getId(), listener.getUserId());
                            if (fullListener != null) {
                                List<Action> actions = fullListener.getLinks();
                                for (Action action : actions) {
                                    restTemplate.getForObject("http://"+action.getType()+"-service/runAction/{id}/{userId}", Status.class, listener.getUserId(), action.getId());
                                }
                            }
                        }
                        DAOFactory.getInstance().getHistoryIdDao().remove(id.getId());
                        id.setUserId(listener.getUserId());
                        id.setHistoryId(fullMessage.getHistoryId());
                    }
                    DAOFactory.getInstance().getHistoryIdDao().create(id);
                }
            }
        }
    }
}
