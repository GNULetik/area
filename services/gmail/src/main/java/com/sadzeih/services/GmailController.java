package com.sadzeih.services;

import com.sadzeih.beans.AccessToken;
import com.sadzeih.beans.GMailAction;
import com.sadzeih.beans.GMailListener;
import com.sadzeih.beans.Users;
import com.sadzeih.dao.AccessTokenDao;
import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.dao.UsersDao;
import com.sadzeih.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by devos-f on 04/01/17.
 */

@RestController
public class GmailController {

    @Autowired
    private GmailAuth auth;

    @Bean
    public GmailAuth newGmailAuth() {
        return new GmailAuth();
    }

    @RequestMapping(path = "/createAction", method = RequestMethod.POST)
    public
    @ResponseBody
    Status createAction(@RequestParam(value = "userId") String userId,
                        @RequestParam(value = "name") String name,
                        @RequestParam(value = "body") String body,
                        @RequestParam(value = "subject") String subject,
                        @RequestParam(value = "destinationAddress") String destinationAddress) {
        GMailAction action = new GMailAction(name, destinationAddress, subject, body);
        DAOFactory.getInstance().getGMailActionDao().create(action, Long.valueOf(userId));
        return new Status("success", "Action created", null);
    }

    @RequestMapping(path = "/findAction", method = RequestMethod.POST)
    public
    @ResponseBody
    Status findAction(@RequestParam(value = "userId") String userId,
                      @RequestParam(value = "id") String id) {
        GMailAction action = DAOFactory.getInstance().getGMailActionDao().find(Long.valueOf(id), Long.valueOf(userId));
        return new Status("success", "Result action", action);
    }

    @RequestMapping(path = "/runAction/{id}/{userId}", method = RequestMethod.GET)
    public @ResponseBody Status runAction(@PathVariable int id,
                                          @PathVariable int userId)
    {
        try {
            ListenersActions.sendMail(DAOFactory.getInstance().getGMailActionDao().find((long)id, (long)userId));
        } catch (IOException e)
        {
            e.printStackTrace();
        } catch (MessagingException e)
        {
            e.printStackTrace();
        }
        return new Status("success", "Mail sent", null);
    }

    @RequestMapping(path = "/createListener", method = RequestMethod.POST)
    public
    @ResponseBody
    Status createListener(@RequestParam(value = "userId") String userId,
                          @RequestParam(value = "topicLabel") String labelName,
                          @RequestParam(value = "topicName") String topicName,
                          @RequestParam(value = "name") String name) {
        GMailListener action = new GMailListener(name, null, labelName, topicName);
        DAOFactory.getInstance().getGMailListenerDao().create(action, Long.valueOf(userId));
        return new Status("success", "Action created", null);
    }

    @RequestMapping(path = "/findListener", method = RequestMethod.POST)
    public
    @ResponseBody
    Status findListener(@RequestParam(value = "userId") String userId,
                        @RequestParam(value = "id") String id) {
        GMailListener action = DAOFactory.getInstance().getGMailListenerDao().find(Long.valueOf(id), Long.valueOf(userId));
        return new Status("success", "Result listener", action);
    }

    @RequestMapping(path = "/callback", method = RequestMethod.POST)
    public
    @ResponseBody
    Status callback(@RequestParam(value="code") String code,
                    @RequestParam(value="state") String state) {
        auth.setCode(code);
        auth.postCode();
        DAOFactory factory = DAOFactory.getInstance();
        AccessTokenDao tokenRepo = factory.getAccessTokenDao();
        AccessToken userToken = tokenRepo.findByToken(state);
        UsersDao userRepo = factory.getUsersDao();
        Users user = userRepo.findById(userToken.getUserId());
        AccessToken findToken = tokenRepo.find(user.getId(), "gmail");
        if (findToken != null) {
            tokenRepo.remove(findToken.getId());
        }
        AccessToken newToken = new AccessToken();
        newToken.setToken(auth.getToken());
        newToken.setUserId(user.getId());
        newToken.setType("gmail");
        tokenRepo.create(newToken);

        AccessToken refreshToken = new AccessToken();
        refreshToken.setToken(auth.getRefreshToken());
        refreshToken.setUserId(user.getId());
        refreshToken.setType("gmailRefresh");
        tokenRepo.create(refreshToken);


        return new Status("success", "GMail authentication", null);
    }

    @RequestMapping(path = "/auth", method = RequestMethod.POST)
    public Status authenticate(@RequestParam(value = "token") String token) {
        return new Status("info", "Authorize URL", auth.buildAuthorizeURL(token));
    }
}
