package com.sadzeih.services;

import com.sadzeih.utils.OAuth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import java.util.Map;

/**
 * Created by devos-f on 04/01/17.
 */

public class GmailAuth extends OAuth {

    private String refreshToken;

    public GmailAuth()
    {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();

        ctx.register(GmailConfig.class);
        ctx.refresh();

        ConfigBean myConfig = ctx.getBean(ConfigBean.class);

        this.clientId = myConfig.getClientId();
        this.secretId = myConfig.getSecretId();
        this.redirectURI = myConfig.getRedirectURI();
        this.authorizeURL = myConfig.getAuthorizeURL();
        this.accessTokenURL = myConfig.getAccessTokenURL();
        this.scope = "https://www.googleapis.com/auth/gmail.modify";
    }

    @Override
    public void setToken(Map<String, Object> result)
    {
        System.out.println(result.toString());
        this.token = (String)result.get("access_token");
        setRefreshToken((String)result.get("refresh_token"));
    }


    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        System.out.println(refreshToken);
        this.refreshToken = refreshToken;
    }
}
