package com.sadzeih.services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;

@EnableAutoConfiguration
@EnableDiscoveryClient
@Import(GmailService.class)
public class GmailServer {
	public static void main(String[] args) {
		System.setProperty("spring.config.name", "gmail-server");

		SpringApplication.run(GmailServer.class, args);
	}
}
