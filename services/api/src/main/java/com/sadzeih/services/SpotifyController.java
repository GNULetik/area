package com.sadzeih.services;

import com.sadzeih.utils.GlobalVariables;
import com.sadzeih.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sadzeih on 04/01/17.
 */
@RestController
@RequestMapping("/spotify")
public class SpotifyController {

    @Autowired
    public RestTemplate restTemplate;

    private static final String url = "http://spotify-service";

    @RequestMapping(path = "/auth", method = RequestMethod.POST)
    public @ResponseBody
    Status authURL(@RequestParam(value = "token") String token)
    {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("token", token);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        return restTemplate.postForObject(url+"/auth", request, Status.class);
    }

    @RequestMapping(path = "/callback", method = RequestMethod.GET)
    public RedirectView callback(@RequestParam(value="code") String code,
                                 @RequestParam(value="state") String state)
    {
        Map<String, String> map = new HashMap<>();
        map.put("code", code);
        map.put("state", state);
        restTemplate.getForObject(url+"/callback/"+ code + "/" + state, Status.class, code,state);
        RedirectView rv = new RedirectView();
        rv.setUrl(GlobalVariables.getFrontUrl());
        return rv;
    }
}
