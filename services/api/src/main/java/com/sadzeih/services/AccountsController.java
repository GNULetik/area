package com.sadzeih.services;

import com.sadzeih.beans.AccessToken;
import com.sadzeih.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sadzeih on 02/01/17.
 */
@RestController
@RequestMapping("/account")
public class AccountsController {

    @Autowired
    public RestTemplate restTemplate;

    private static final String url = "http://accounts-service";

    @RequestMapping(path="/register", method= RequestMethod.POST)
    public @ResponseBody
    Status register(@RequestParam(value="email") String email,
                    @RequestParam(value="hashpassword") String hashpassword)
    {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
        map.add("email", email);
        map.add("hashpassword", hashpassword);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

        return restTemplate.postForObject(url+"/register", request, Status.class);
    }

    @RequestMapping(path="/connect", method= RequestMethod.POST)
    public @ResponseBody
    Status connect(@RequestParam(value="email") String email,
                    @RequestParam(value="hashpassword") String hashpassword)
    {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
        map.add("email", email);
        map.add("hashpassword", hashpassword);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

        return restTemplate.postForObject(url+"/connect", request, Status.class);
    }

    @RequestMapping(path="/disconnect/{id}")
    public @ResponseBody
    Status disconnect(@PathVariable String id)
    {
        return restTemplate.getForObject(url+"/disconnect/{id}", Status.class, id);
    }

    @RequestMapping(path="/get/{id}")
    public @ResponseBody
    Status getAccount(@PathVariable String id)
    {
        return restTemplate.getForObject(url+"/get/{id}", Status.class, id);
    }

    @RequestMapping(path="/remove/{id}")
    public @ResponseBody
    Status remove(@PathVariable String id)
    {
        return restTemplate.getForObject(url+"/remove/{id}", Status.class, id);
    }
}
