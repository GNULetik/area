package com.sadzeih.services;

import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

/**
 * Created by sadzeih on 04/01/17.
 */
@RestController
@RequestMapping("/action")
public class ActionController {

    @Autowired
    public RestTemplate restTemplate;

    private static final String url = "http://action-service";
    @RequestMapping(path = "/getAll", method = RequestMethod.POST)
    public
    @ResponseBody
    Status getAll(@RequestParam(value = "token") String token) {
        Long userId = DAOFactory.getInstance().getAccessTokenDao().findByToken(token).getUserId();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("userId", String.valueOf(userId));
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        return restTemplate.postForObject(url+"/getAll", request, Status.class);
    }

    @RequestMapping(path = "/delete", method = RequestMethod.POST)
    public Status delete(@RequestParam(value = "id") String id, @RequestParam(value = "token") String token) {
        Long userId = DAOFactory.getInstance().getAccessTokenDao().findByToken(token).getUserId();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("id", id);
        map.add("userId", String.valueOf(userId));
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        ResponseEntity<Status> res = restTemplate.exchange(url+ "/delete", HttpMethod.POST, request, Status.class, new HashMap<String, String>());
        return res.getBody();
    }


    @RequestMapping(path = "/find", method = RequestMethod.POST)
    public Status find(@RequestParam(value = "id") String id, @RequestParam(value = "token") String token) {
        Long userId = DAOFactory.getInstance().getAccessTokenDao().findByToken(token).getUserId();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("id", id);
        map.add("userId", String.valueOf(userId));
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        ResponseEntity<Status> res = restTemplate.exchange(url+ "/find", HttpMethod.POST, request, Status.class, new HashMap<String, String>());
        return res.getBody();
    }
}
