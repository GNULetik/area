package com.sadzeih.services;

import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.utils.GlobalVariables;
import com.sadzeih.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.view.RedirectView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sadzeih on 04/01/17.
 */
@RestController
@RequestMapping("/youtube")
public class YoutubeController {

    @Autowired
    public RestTemplate restTemplate;

    private static final String url = "http://youtube-service";

    @RequestMapping(path = "/createAction", method = RequestMethod.POST)
    public @ResponseBody
    Status createAction(@RequestParam(value = "token") String token,
                        @RequestParam(value = "name") String name,
                        @RequestParam(value = "video") String video)
    {
        Long userId = DAOFactory.getInstance().getAccessTokenDao().findByToken(token).getUserId();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("userId", String.valueOf(userId));
        map.add("name", name);
        map.add("video", video);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        return restTemplate.postForObject(url+"/createAction", request, Status.class);
    }

    @RequestMapping(path = "/createListener", method = RequestMethod.POST)
    public @ResponseBody
    Status createListener(@RequestParam(value = "token") String token,
                          @RequestParam(value = "name") String name)
    {
        Long userId = DAOFactory.getInstance().getAccessTokenDao().findByToken(token).getUserId();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("userId", String.valueOf(userId));
        map.add("name", name);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        return restTemplate.postForObject(url+"/createListener", request, Status.class);
    }

    @RequestMapping(path = "/findAction", method = RequestMethod.POST)
    public @ResponseBody
    Status findAction(@RequestParam(value = "token") String token,
                      @RequestParam(value = "id") String id)
    {
        Long userId = DAOFactory.getInstance().getAccessTokenDao().findByToken(token).getUserId();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("userId", String.valueOf(userId));
        map.add("id", id);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        return restTemplate.postForObject(url+"/findAction", request, Status.class);
    }

    @RequestMapping(path = "/findListener", method = RequestMethod.POST)
    public @ResponseBody
    Status findListener(@RequestParam(value = "token") String token,
                        @RequestParam(value = "id") String id)
    {
        Long userId = DAOFactory.getInstance().getAccessTokenDao().findByToken(token).getUserId();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("userId", String.valueOf(userId));
        map.add("id", id);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        return restTemplate.postForObject(url+"/findListener", request, Status.class);
    }

    @RequestMapping(path = "/auth", method = RequestMethod.POST)
    public @ResponseBody
    Status authURL(@RequestParam(value = "token") String token)
    {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("token", token);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        return restTemplate.postForObject(url+"/auth", request, Status.class);
    }

    @RequestMapping(path = "/callback", method = RequestMethod.GET)
    public @ResponseBody
    RedirectView callback(@RequestParam(value="code") String code,
                          @RequestParam(value="state") String state)
    {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("code", code);
        map.add("state", state);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        restTemplate.postForObject(url+"/callback", request, Status.class);
        RedirectView rv = new RedirectView();
        rv.setUrl(GlobalVariables.getFrontUrl());
        return rv;
    }
}
