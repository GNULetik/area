package com.sadzeih.services;

import com.sadzeih.utils.GlobalVariables;
import com.sadzeih.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Created by sadzeih on 04/01/17.
 */
@RestController
@RequestMapping("/twitter")
public class TwitterController {

    @Autowired
    public RestTemplate restTemplate;

    private static final String url = "http://twitter-service";

    @RequestMapping(path = "/auth", method = RequestMethod.POST)
    public @ResponseBody
    Status authURL(@RequestParam(value = "token") String token)
    {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("token", token);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        return restTemplate.postForObject(url+"/auth", request, Status.class);
    }

    @RequestMapping(path = "/callback", method = RequestMethod.GET)
    public RedirectView callback(@RequestParam(value="oauth_token") String token,
                                 @RequestParam(value="oauth_verifier") String verifier)
    {
        restTemplate.getForObject(url+"/callback/"+ token + "/" + verifier, Status.class, token, verifier);
        RedirectView rv = new RedirectView();
        rv.setUrl(GlobalVariables.getFrontUrl());
        return rv;
    }
}
