package com.sadzeih.services;

import com.sadzeih.beans.DropboxListener;
import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.utils.GlobalVariables;
import com.sadzeih.utils.Status;
import jdk.nashorn.internal.ir.RuntimeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sadzeih on 04/01/17.
 */
@RestController
@RequestMapping("/dropbox")
public class DropboxController {

    @Autowired
    public RestTemplate restTemplate;

    private static final String url = "http://dropbox-service";

    @RequestMapping(path = "/auth", method = RequestMethod.POST)
    public @ResponseBody
    Status authURL(@RequestParam(value = "token") String token)
    {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("token", token);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        return restTemplate.postForObject(url+"/auth", request, Status.class);
    }

    @RequestMapping(path = "/webhook", method = RequestMethod.GET)
    public @ResponseBody
    String webhookCheck(@RequestBody String challenge) {
        return challenge;
    }

    @RequestMapping(path = "/webhook", method = RequestMethod.POST)
    public @ResponseBody
    String webhook(@RequestBody String json) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(json, headers);
        return restTemplate.postForObject(url+"/webhook", request, String.class);
    }

    @RequestMapping(path = "/callback", method = RequestMethod.GET)
    public RedirectView callback(@RequestParam(value="code") String code,
                    @RequestParam(value="state") String state)
    {
        Map<String, String> map = new HashMap<>();
        map.put("code", code);
        map.put("state", state);

        restTemplate.getForObject(url+"/callback/" + code + "/" + state, Status.class, code,state);
        RedirectView rv = new RedirectView();
        rv.setUrl(GlobalVariables.getFrontUrl());
        return rv;
    }

    @RequestMapping(path = "/createListener", method = RequestMethod.POST)
    public
    @ResponseBody
    Status createListener(@RequestParam(value = "token") String token,
                          @RequestParam(value = "name") String name) {
        Long userId = DAOFactory.getInstance().getAccessTokenDao().findByToken(token).getUserId();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        LinkedMultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("userId", String.valueOf(userId));
        map.add("name", name);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        return restTemplate.postForObject(url+"/createListener", request, Status.class);
    }

    @RequestMapping(path = "/createAction", method = RequestMethod.POST)
    public
    @ResponseBody
    Status createAction(@RequestParam(value = "token") String token,
                          @RequestParam(value = "name") String name) {
        Long userId = DAOFactory.getInstance().getAccessTokenDao().findByToken(token).getUserId();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        LinkedMultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("userId", String.valueOf(userId));
        map.add("name", name);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        return restTemplate.postForObject(url+"/createAction", request, Status.class);
    }
}
