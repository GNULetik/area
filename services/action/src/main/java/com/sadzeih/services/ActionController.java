package com.sadzeih.services;

import com.sadzeih.beans.AccessToken;
import com.sadzeih.beans.Action;
import com.sadzeih.beans.Users;
import com.sadzeih.dao.AccessTokenDao;
import com.sadzeih.dao.ActionDao;
import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.dao.UsersDao;
import com.sadzeih.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.List;

/**
 * Created by devos-f on 04/01/17.
 */

@RestController
public class ActionController {
    @RequestMapping(path = "/getAll", method = RequestMethod.POST)
    public
    @ResponseBody
    Status getAll(@RequestParam(value = "userId") String userId) {
        DAOFactory factory = DAOFactory.getInstance();
        ActionDao listener = factory.getActionDao();
        List<Action> listeners = listener.getAll(Long.parseLong(userId));
        return (new Status("success", "Gmail authentication", listeners));
    }

    @RequestMapping(path = "/delete", method = RequestMethod.POST)
    public Status delete(@RequestParam(value = "id") String id, @RequestParam(value = "userId") String userId) {
        DAOFactory factory = DAOFactory.getInstance();
        ActionDao listener = factory.getActionDao();
        listener.remove(Long.parseLong(id), Long.parseLong(userId));
        return new Status("success", "delete ok", null);
    }


    @RequestMapping(path = "/find", method = RequestMethod.POST)
    public Status find(@RequestParam(value = "id") String id, @RequestParam(value = "userId") String userId) {
        DAOFactory factory = DAOFactory.getInstance();
        ActionDao listenerDao = factory.getActionDao();
        Action listener = listenerDao.find(Long.parseLong(id), Long.parseLong(userId));
        return new Status("success", "Result", listener);
    }
}
