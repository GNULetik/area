package com.sadzeih.services;

import com.sadzeih.beans.AccessToken;
import com.sadzeih.beans.Listener;
import com.sadzeih.beans.Users;
import com.sadzeih.dao.AccessTokenDao;
import com.sadzeih.dao.ListenerDao;
import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.dao.UsersDao;
import com.sadzeih.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.List;

/**
 * Created by devos-f on 04/01/17.
 */

@RestController
public class ListenerController {
    @RequestMapping(path = "/getAll", method = RequestMethod.POST)
    public
    @ResponseBody
    Status getAll(@RequestParam(value = "userId") String userId) {
        DAOFactory factory = DAOFactory.getInstance();
        ListenerDao listener = factory.getListenerDao();
        List<Listener> listeners = listener.getAll(Long.parseLong(userId));
        return (new Status("success", "Result", listeners));
    }

    @RequestMapping(path = "/delete", method = RequestMethod.POST)
    public Status delete(@RequestParam(value = "id") String id, @RequestParam(value = "userId") String userId) {
        DAOFactory factory = DAOFactory.getInstance();
        ListenerDao listener = factory.getListenerDao();
        listener.remove(Long.parseLong(id), Long.parseLong(userId));
        return new Status("success", "delete ok", null);
    }


    @RequestMapping(path = "/find", method = RequestMethod.POST)
    public Status find(@RequestParam(value = "id") String id, @RequestParam(value = "userId") String userId) {
        DAOFactory factory = DAOFactory.getInstance();
        ListenerDao listenerDao = factory.getListenerDao();
        Listener listener = listenerDao.find(Long.parseLong(id), Long.parseLong(userId));
        return new Status("success", "Result", listener);
    }

    @RequestMapping(path = "/link", method = RequestMethod.POST)
    public
    @ResponseBody
    Status addLink(@RequestParam(value = "userId") String userId, @RequestParam(value="listenerId") String listenerId,
                   @RequestParam(value = "actionId") String actionId) {
        DAOFactory factory = DAOFactory.getInstance();
        ListenerDao listener = factory.getListenerDao();
        listener.addAction(Long.parseLong(listenerId), Long.parseLong(actionId),
                Long.parseLong(userId));
        return (new Status("success", "Link added", null));
    }

    @RequestMapping(path = "/deleteLink", method = RequestMethod.POST)
    public
    @ResponseBody
    Status removeLink(@RequestParam(value = "userId") String userId, @RequestParam(value="listenerId") String listenerId,
                      @RequestParam(value = "actionId") String actionId) {
        DAOFactory factory = DAOFactory.getInstance();
        ListenerDao listener = factory.getListenerDao();
        listener.removeAction(Long.parseLong(listenerId), Long.parseLong(actionId),
                Long.parseLong(userId));
        return (new Status("success", "Link removed", null));
    }
}
