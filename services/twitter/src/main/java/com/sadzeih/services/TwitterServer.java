package com.sadzeih.services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;

@EnableAutoConfiguration
@EnableDiscoveryClient
@Import(TwitterService.class)
public class TwitterServer {
	public static void main(String[] args) {
		System.setProperty("spring.config.name", "twitter-server");

		SpringApplication.run(TwitterServer.class, args);
	}
}
