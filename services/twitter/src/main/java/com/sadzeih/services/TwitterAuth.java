package com.sadzeih.services;

import com.sadzeih.beans.OAuthToken;
import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.dao.OAuthTokenDao;
import com.sadzeih.utils.OAuth;
import org.apache.commons.codec.binary.Base64;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.ExceptionTypeFilter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.util.Calendar;
import java.util.Map;
import java.util.UUID;

/**
 * Created by devos-f on 04/01/17.
 */

public class TwitterAuth extends OAuth {
    private String requestToken;
    private String preToken;
    private String tokenSecret;
    private String tokenVerifier;

    public TwitterAuth()
    {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();

        ctx.register(TwitterConfig.class);
        ctx.refresh();

        ConfigBean myConfig = ctx.getBean(ConfigBean.class);

        this.clientId = myConfig.getClientId();
        this.secretId = myConfig.getSecretId();
        this.redirectURI = myConfig.getRedirectURI();
        this.authorizeURL = myConfig.getAuthorizeURL();
        this.requestToken = myConfig.getRequestToken();
        this.accessTokenURL = myConfig.getAccessTokenURL();
    }

    public String encode(String value)
    {
        String encoded = null;
        try {
            encoded = URLEncoder.encode(value, "UTF-8");
        } catch (UnsupportedEncodingException ignore) {
        }
        StringBuilder buf = new StringBuilder(encoded.length());
        char focus;
        for (int i = 0; i < encoded.length(); i++) {
            focus = encoded.charAt(i);
            if (focus == '*') {
                buf.append("%2A");
            } else if (focus == '+') {
                buf.append("%20");
            } else if (focus == '%' && (i + 1) < encoded.length()
                    && encoded.charAt(i + 1) == '7' && encoded.charAt(i + 2) == 'E') {
                buf.append('~');
                i += 2;
            } else {
                buf.append(focus);
            }
        }
        return buf.toString();
    }

    private static String computeSignature(String baseString, String keyString) throws GeneralSecurityException, UnsupportedEncodingException
    {
        SecretKey secretKey = null;

        byte[] keyBytes = keyString.getBytes();
        secretKey = new SecretKeySpec(keyBytes, "HmacSHA1");

        Mac mac = Mac.getInstance("HmacSHA1");
        mac.init(secretKey);

        byte[] text = baseString.getBytes();

        return new String(Base64.encodeBase64(mac.doFinal(text))).trim();
    }

    private String buildAuthHeader()
    {
        String uuid_string = UUID.randomUUID().toString();
        uuid_string = uuid_string.replaceAll("-", "");
        String oauth_nonce = uuid_string;
        Calendar tempcal = Calendar.getInstance();
        long ts = tempcal.getTimeInMillis();
        String oauth_timestamp = (new Long(ts/1000)).toString();
        String oauth_signature_method = "HMAC-SHA1";
        String header_string = "oauth_consumer_key="+this.clientId+"&oauth_nonce="+oauth_nonce+"&";
        header_string += "oauth_signature_method="+oauth_signature_method+"&";
        header_string += "oauth_timestamp="+oauth_timestamp+"&";
        header_string += "oauth_version=1.0";

        String signature_base_string = "POST&"+encode(requestToken)+"&"+encode(header_string);
        String signature = "";
        try {
            signature = computeSignature(signature_base_string, secretId+"&");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        header_string = "oauth_consumer_key=\""+this.clientId+"\",";
        header_string += "oauth_nonce=\""+oauth_nonce+"\",";
        header_string += "oauth_signature_method=\""+oauth_signature_method+"\",";
        header_string += "oauth_timestamp=\""+oauth_timestamp+"\",";
        header_string += "oauth_signature=\""+encode(signature)+"\",";
        header_string += "oauth_version=\"1.0\"";

        return header_string;
    }

    private String buildAuthHeaderWithToken(String token)
    {
        String uuid_string = UUID.randomUUID().toString();
        uuid_string = uuid_string.replaceAll("-", "");
        String oauth_nonce = uuid_string;
        Calendar tempcal = Calendar.getInstance();
        long ts = tempcal.getTimeInMillis();
        String oauth_timestamp = (new Long(ts/1000)).toString();
        String oauth_signature_method = "HMAC-SHA1";
        String header_string = "oauth_consumer_key="+this.clientId+"&oauth_nonce="+oauth_nonce+"&";
        header_string += "oauth_signature_method="+oauth_signature_method+"&";
        header_string += "oauth_timestamp="+oauth_timestamp+"&";
        header_string += "oauth_token="+encode(token)+"&";
        header_string += "oauth_version=1.0";

        String signature_base_string = "POST&"+encode(accessTokenURL)+"&"+encode(header_string);
        String signature = "";
        try {
            signature = computeSignature(signature_base_string, secretId+"&");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        header_string = "oauth_consumer_key=\""+this.clientId+"\",";
        header_string += "oauth_nonce=\""+oauth_nonce+"\",";
        header_string += "oauth_signature_method=\""+oauth_signature_method+"\",";
        header_string += "oauth_timestamp=\""+oauth_timestamp+"\",";
        header_string += "oauth_signature=\""+encode(signature)+"\",";
        header_string += "oauth_token=\""+encode(token)+"\",";
        header_string += "oauth_version=\"1.0\"";

        return header_string;
    }

    @Override
    public String buildAuthorizeURL(String state)
    {
        String header_string = "OAuth ";
        header_string += buildAuthHeader();
        System.out.println(header_string);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", header_string);

        RestTemplate restTemplate = restTemplate();
        HttpEntity<String> request = new HttpEntity<String>("", headers);
        String tokens = restTemplate.postForObject(requestToken, request, String.class);
        String[] TokenSplit = tokens.split("&");
        String pretoken = TokenSplit[0].split("=")[1];
        DAOFactory factory = DAOFactory.getInstance();
        OAuthTokenDao repo = factory.getOAuthTokenDao();
        OAuthToken token = new OAuthToken();
        token.setLocalToken(state);
        token.setExternToken(pretoken);
        repo.create(token);
        return this.authorizeURL+"?oauth_token="+pretoken;
    }

    private void setPreToken(Map<String, Object> result)
    {
        this.preToken = (String)result.get("oauth_token");
    }

    public void setSecretToken(Map<String, Object> result)
    {
        this.tokenSecret = (String)result.get("oauth_token_secret");
    }

    public void requestAccessToken(String verifier, String token)
    {
        String header_string = "OAuth ";
        header_string += buildAuthHeaderWithToken(token);
        String post_body = "oauth_verifier="+encode(verifier);
        System.out.println(header_string);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", header_string);
        headers.set("Content-Type", "application/x-www-form-urlencoded");
        headers.set("Content-Length", Integer.toString(post_body.length()));
        RestTemplate restTemplate = restTemplate();
        HttpEntity<String> request = new HttpEntity<String>(post_body, headers);
        String tokens = restTemplate.postForObject(accessTokenURL, request, String.class);
        String[] TokenSplit = tokens.split("&");
        setToken(TokenSplit[0].split("=")[1]);
        setTokenSecret(TokenSplit[1].split("=")[1]);
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public String getRequestToken() {
        return requestToken;
    }

    public void setRequestToken(String requestToken) {
        this.requestToken = requestToken;
    }

    public String getTokenSecret() {
        return tokenSecret;
    }

    public void setTokenSecret(String tokenSecret) {
        this.tokenSecret = tokenSecret;
    }

    public String getPreToken() {
        return preToken;
    }

    public void setPreToken(String preToken) {
        this.preToken = preToken;
    }

    public String getTokenVerifier() {
        return tokenVerifier;
    }

    public void setTokenVerifier(String tokenVerifier) {
        this.tokenVerifier = tokenVerifier;
    }
}
