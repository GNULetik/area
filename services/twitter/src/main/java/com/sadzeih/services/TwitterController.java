package com.sadzeih.services;

import com.sadzeih.beans.AccessToken;
import com.sadzeih.beans.OAuthToken;
import com.sadzeih.dao.AccessTokenDao;
import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.dao.OAuthTokenDao;
import com.sadzeih.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;

/**
 * Created by devos-f on 04/01/17.
 */

@RestController
public class TwitterController {

    @Autowired
    private TwitterAuth auth;

    @Bean
    public TwitterAuth newTwitterAuth() {
        return new TwitterAuth();
    }

    @RequestMapping(path = "/callback/{token}/{verifier}", method = RequestMethod.GET)
    public
    @ResponseBody
    Status callback(@PathVariable String token,
                    @PathVariable String verifier) {
        DAOFactory factory = DAOFactory.getInstance();
        OAuthTokenDao tokenRepo = factory.getOAuthTokenDao();
        OAuthToken twitterToken = tokenRepo.find(token);
        if (twitterToken.getExternToken().equals(token))
        {
            auth.requestAccessToken(verifier, token);
            AccessTokenDao accessTokenRepo = factory.getAccessTokenDao();
            AccessToken secret = new AccessToken();
            AccessToken userToken = accessTokenRepo.findByToken(twitterToken.getLocalToken());
            secret.setToken(auth.getTokenSecret());
            secret.setUserId(userToken.getUserId());
            secret.setType("twitter-secret");
            accessTokenRepo.create(secret);
            AccessToken normalToken = new AccessToken();
            normalToken.setType("twitter-token");
            normalToken.setToken(auth.getToken());
            normalToken.setUserId(userToken.getUserId());
            accessTokenRepo.create(normalToken);
        }
        return (new Status("success", "Twitter authentication", null));
    }

    @RequestMapping(path = "/auth", method = RequestMethod.POST)
    public Status authenticate(@RequestParam(value = "token") String token) {
        return new Status("info", "Authorize URL", auth.buildAuthorizeURL(token));
    }
}
