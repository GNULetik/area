package com.sadzeih.services;

import com.sadzeih.utils.GlobalVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;

/**
 * Created by devos-f on 10/01/17.
 */

@Configuration
@PropertySource("classpath:twitter.properties")
public class TwitterConfig {

    @Autowired
    Environment env;

    @Bean
    public ConfigBean config()
    {
        ConfigBean config = new ConfigBean();
        config.setClientId(env.getProperty("auth.clientId"));
        config.setSecretId(env.getProperty("auth.secretId"));
        config.setRedirectURI(GlobalVariables.getAPIUrl() + "/twitter/callback");
        config.setAuthorizeURL(env.getProperty("auth.authorizeURL"));
        config.setAccessTokenURL(env.getProperty("auth.accessTokenURL"));
        config.setRequestToken(env.getProperty("auth.requestToken"));

        return (config);
    }
}

