package com.sadzeih.services;

import com.sadzeih.beans.AccessToken;
import com.sadzeih.beans.Users;
import com.sadzeih.dao.AccessTokenDao;
import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.dao.UsersDao;
import com.sadzeih.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

/**
 * Created by devos-f on 04/01/17.
 */

@RestController
public class SpotifyController {

    @Autowired
    private SpotifyAuth auth;

    @Bean
    public SpotifyAuth newSpotifyAuth() {
        return new SpotifyAuth();
    }

    @RequestMapping(path = "/callback/{code}/{state}", method = RequestMethod.GET)
    public
    @ResponseBody
    Status callback(@PathVariable String code,
                    @PathVariable String state) {

        try {
            auth.setCode(code);
            auth.postCode();
            DAOFactory factory = DAOFactory.getInstance();
            AccessTokenDao tokenRepo = factory.getAccessTokenDao();
            AccessToken userToken = tokenRepo.findByToken(state);
            UsersDao userRepo = factory.getUsersDao();
            Users user = userRepo.findById(userToken.getUserId());
            AccessToken findToken = tokenRepo.find(user.getId(), "spotify");
            if (findToken != null && findToken.getExpirationDate().getTime() <= new Date().getTime()) {
                tokenRepo.remove(findToken.getId());
            }
            AccessToken newToken = new AccessToken();
            newToken.setToken(auth.getToken());
            newToken.setUserId(user.getId());
            newToken.setType("spotify");
            tokenRepo.create(newToken);

            return (new Status("success", "Spotify authentication", null));
        } catch (Exception e){
            return (new Status("error", "Spotify authentication failed", null));
        }
    }

    @RequestMapping(path = "/auth", method = RequestMethod.POST)
    public Status authenticate(@RequestParam(value = "token") String token) {
        return new Status("info", "Authorize URL", auth.buildAuthorizeURL(token));
    }
}
