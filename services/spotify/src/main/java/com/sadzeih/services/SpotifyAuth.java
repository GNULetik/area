package com.sadzeih.services;

import com.sadzeih.utils.OAuth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

/**
 * Created by devos-f on 04/01/17.
 */

public class SpotifyAuth extends OAuth {


    public SpotifyAuth()
    {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();

        ctx.register(SpotifyConfig.class);
        ctx.refresh();

        ConfigBean myConfig = ctx.getBean(ConfigBean.class);

        this.clientId = myConfig.getClientId();
        this.secretId = myConfig.getSecretId();
        this.redirectURI = myConfig.getRedirectURI();
        this.authorizeURL = myConfig.getAuthorizeURL();
        this.accessTokenURL = myConfig.getAccessTokenURL();
    }

}
