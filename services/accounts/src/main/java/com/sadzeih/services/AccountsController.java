package com.sadzeih.services;

import com.sadzeih.beans.AccessToken;
import com.sadzeih.beans.Users;
import com.sadzeih.dao.AccessTokenDao;
import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.dao.UsersDao;
import com.sadzeih.utils.Status;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Path;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.Calendar;
import java.text.SimpleDateFormat;

/**
 * Created by sadzeih on 02/01/17.
 */

@RestController
public class AccountsController {

    protected SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    protected Calendar cal = Calendar.getInstance();

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public @ResponseBody
    Status register(@RequestParam(value="email") String email,
                    @RequestParam(value="hashpassword") String hashpassword)
    {
        Users user = new Users();
        user.setEmail(email);
        user.setPassword(hashpassword);
        UsersDao repo;
        DAOFactory daoFactory = DAOFactory.getInstance();
        repo = daoFactory.getUsersDao();
        if (repo.findByMail(email) == null)
            repo.create(user);
        else
            return new Status("error", "Email already registered.", null);
        AccessToken token = new AccessToken();
        token.setType("local");
        user = repo.findByMail(email);
        token.setUserId(user.getId());
        token.setToken(UUID.randomUUID().toString());
        AccessTokenDao tokenRepo = daoFactory.getAccessTokenDao();
        tokenRepo.create(token);
        return new Status("success", "Account created.", tokenRepo.find(user.getId(), "local"));
    }

    @RequestMapping(path = "/connect", method = RequestMethod.POST)
    public @ResponseBody
    Status connect(@RequestParam(value="email") String email,
                   @RequestParam(value="hashpassword") String hashpassword)
    {
        UsersDao userRepo;
        AccessTokenDao tokenRepo;
        DAOFactory factory = DAOFactory.getInstance();
        userRepo = factory.getUsersDao();
        tokenRepo = factory.getAccessTokenDao();
        Users user = userRepo.findByMail(email);
        if (user != null)
        {
            if (user.getPassword().equals(hashpassword)) {
                AccessToken token = tokenRepo.find(user.getId(), "local");
                if (token != null && token.getExpirationDate().getTime() <= new Date().getTime()) {
                    tokenRepo.remove(token.getId());
                    AccessToken newToken = new AccessToken();
                    newToken.setUserId(user.getId());
                    newToken.setType("local");
                    newToken.setToken(UUID.randomUUID().toString());
                    tokenRepo.create(newToken);
                } else {
                    AccessToken newToken = new AccessToken();
                    newToken.setUserId(user.getId());
                    newToken.setType("local");
                    newToken.setToken(UUID.randomUUID().toString());
                    tokenRepo.create(newToken);		    
		}
                return new Status("success", "Connected.", tokenRepo.find(user.getId(), "local"));
            }
            else
                return new Status("error", "Wrong password.", null);
        }
        else
            return new Status("error", "No account with this email.", null);
    }

    @RequestMapping(path = "/disconnect/{id}", method = RequestMethod.GET)
    public @ResponseBody
    Status disconnect(@PathVariable int id)
    {
        DAOFactory factory = DAOFactory.getInstance();
        UsersDao userRepo = factory.getUsersDao();
        AccessTokenDao tokenRepo = factory.getAccessTokenDao();
        Users user = userRepo.findById((long)id);
        if (user == null)
            return new Status("error", "User not found.", null);
        AccessToken token = tokenRepo.find(user.getId(), "local");
        tokenRepo.remove(token.getId());
        return new Status("success", "Disconnected.", null);
    }

    @RequestMapping(path = "/get/{id}")
    public @ResponseBody
    Status getAccount(@PathVariable int id)
    {
        DAOFactory factory = DAOFactory.getInstance();
        UsersDao userRepo = factory.getUsersDao();
        Users user = userRepo.findById((long)id);
        if (user == null)
            return new Status("error", "User not found.", null);
        return new Status("success", "User found.", user);
    }

    @RequestMapping(path = "/remove/{id}")
    public @ResponseBody
    Status remove(@PathVariable int id)
    {
        DAOFactory factory = DAOFactory.getInstance();
        UsersDao userRepo = factory.getUsersDao();
        AccessTokenDao tokenRepo = factory.getAccessTokenDao();
        Users user = userRepo.findById((long)id);
        if (user == null)
            return new Status("error", "User not found.", null);
        userRepo.remove((long)id);
        List<AccessToken> tokens = tokenRepo.findAll(user.getId());
        for (AccessToken token: tokens) {
            tokenRepo.remove(token.getId());
        }
        return new Status("success", "User deleted.", user);
    }
}
