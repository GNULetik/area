package com.sadzeih.services;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.*;
import com.sadzeih.beans.*;
import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

/**
 * Created by sadzeih on 2/4/17.
 */
@Component
public class ListenersActions {

    @Autowired
    public RestTemplate restTemplate;

    private static final JsonFactory JsonFactory = JacksonFactory.getDefaultInstance();
    private static HttpTransport HttpTransport;

    static {
        try {
            HttpTransport = GoogleNetHttpTransport.newTrustedTransport();
        }
        catch (GeneralSecurityException se)
        {
            se.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public ListenersActions()
    {
        List<YoutubeListener> listeners = DAOFactory.getInstance().getYoutubeListenerDao().findAll();
        for (YoutubeListener listener: listeners) {
            AccessToken token = DAOFactory.getInstance().getAccessTokenDao().find(listener.getUserId(), "youtube");
            AccessToken refreshToken = DAOFactory.getInstance().getAccessTokenDao().find(listener.getUserId(), "youtubeRefresh");
            if (token != null && refreshToken != null) {
                YoutubeAuth auth = new YoutubeAuth();
                GoogleCredential credential = new GoogleCredential.Builder().setTransport(HttpTransport).setJsonFactory(JsonFactory).setClientSecrets(auth.getClientId(), auth.getSecretId()).build();
                credential.setAccessToken(token.getToken());
                credential.setRefreshToken(refreshToken.getToken());
                YouTube service = getYoutubeService(credential);
                try {
                    fullSync(token.getUserId(), service);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static YouTube getYoutubeService(GoogleCredential credential)
    {
        return new YouTube.Builder(HttpTransport, JsonFactory, credential).setApplicationName("Area").build();
    }

    public void fullSync(Long userId, YouTube service) throws IOException
    {
        ChannelListResponse response = service.channels().list("contentDetails").setMine(true).execute();
        List<Channel> channels = response.getItems();
        Channel channel = channels.get(0);
        String likesId = channel.getContentDetails().getRelatedPlaylists().getLikes();
        PlaylistItemListResponse playlistItemListResponse = service.playlistItems().list("snippet,contentDetails,status").setPlaylistId(likesId).execute();
        YoutubeLikes likes = DAOFactory.getInstance().getYoutubeLikesDao().find(userId);
        if (likes != null) {
            DAOFactory.getInstance().getYoutubeLikesDao().remove(likes.getId());
        }
        likes = new YoutubeLikes();
        likes.setUserId(userId);
        likes.setLikes(Long.valueOf(playlistItemListResponse.getPageInfo().getTotalResults()));
        DAOFactory.getInstance().getYoutubeLikesDao().create(likes);
    }

    static public void addWatchlist(YoutubeAction action) throws IOException
    {
        AccessToken token = DAOFactory.getInstance().getAccessTokenDao().find(action.getUserId(), "youtube");
        AccessToken refreshToken = DAOFactory.getInstance().getAccessTokenDao().find(action.getUserId(), "youtubeRefresh");
        YoutubeAuth auth = new YoutubeAuth();
        GoogleCredential credential = new GoogleCredential.Builder().setTransport(HttpTransport).setJsonFactory(JsonFactory).setClientSecrets(auth.getClientId(), auth.getSecretId()).build();
        credential.setAccessToken(token.getToken());
        credential.setRefreshToken(refreshToken.getToken());
        YouTube service = getYoutubeService(credential);

        ChannelListResponse response = service.channels().list("contentDetails").setMine(true).execute();
        List<Channel> channels = response.getItems();
        Channel channel = channels.get(0);
        String watchlistId = channel.getContentDetails().getRelatedPlaylists().getWatchLater();
        PlaylistItem item = new PlaylistItem();
        PlaylistItemSnippet snippet = new PlaylistItemSnippet();
        snippet.setPlaylistId(watchlistId);
        ResourceId resourceId = new ResourceId();
        resourceId.setKind("youtube#video");
        resourceId.setVideoId(action.getVideo());
        snippet.setResourceId(resourceId);
        item.setSnippet(snippet);
        service.playlistItems().insert("snippet", item).execute();
    }

    @Scheduled(fixedRate = 5000)
    public void listening() throws IOException
    {
        List<YoutubeListener> listeners = DAOFactory.getInstance().getYoutubeListenerDao().findAll();
        for (YoutubeListener listener: listeners)
        {
            AccessToken token = DAOFactory.getInstance().getAccessTokenDao().find(listener.getUserId(), "youtube");
            AccessToken refreshToken = DAOFactory.getInstance().getAccessTokenDao().find(listener.getUserId(), "youtubeRefresh");
            YoutubeAuth auth = new YoutubeAuth();
            GoogleCredential credential = new GoogleCredential.Builder().setTransport(HttpTransport).setJsonFactory(JsonFactory).setClientSecrets(auth.getClientId(), auth.getSecretId()).build();
            credential.setAccessToken(token.getToken());
            credential.setRefreshToken(refreshToken.getToken());
            YouTube service = getYoutubeService(credential);
            if (listener.getType().equals("youtube"))
            {
                ChannelListResponse response = service.channels().list("contentDetails").setMine(true).execute();
                List<Channel> channels = response.getItems();
                Channel channel = channels.get(0);
                String likesId = channel.getContentDetails().getRelatedPlaylists().getLikes();
                PlaylistItemListResponse playlistItemListResponse = service.playlistItems().list("snippet,contentDetails,status").setPlaylistId(likesId).execute();
                Long nbLikes = Long.valueOf(playlistItemListResponse.getPageInfo().getTotalResults());
                YoutubeLikes likes = DAOFactory.getInstance().getYoutubeLikesDao().find(listener.getUserId());
                if (!nbLikes.equals(likes.getLikes()))
                {
                    System.out.println(nbLikes);
                    YoutubeListener fullListener = DAOFactory.getInstance().getYoutubeListenerDao().find(listener.getId(), listener.getUserId());
                    if (fullListener != null) {
                        List<Action> actions = fullListener.getLinks();
                        for (Action action : actions) {
                            restTemplate.getForObject("http://"+action.getType()+"-service/runAction/{id}/{userId}", Status.class, listener.getUserId(), action.getId());
                        }
                    }
                    DAOFactory.getInstance().getYoutubeLikesDao().remove(likes.getId());
                    YoutubeLikes newLikes = new YoutubeLikes();
                    newLikes.setUserId(listener.getUserId());
                    newLikes.setLikes(nbLikes);
                    DAOFactory.getInstance().getYoutubeLikesDao().create(newLikes);
                }
            }
        }
    }
}
