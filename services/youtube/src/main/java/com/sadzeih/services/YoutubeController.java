package com.sadzeih.services;

import com.sadzeih.beans.AccessToken;
import com.sadzeih.beans.Users;
import com.sadzeih.beans.YoutubeAction;
import com.sadzeih.beans.YoutubeListener;
import com.sadzeih.dao.AccessTokenDao;
import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.dao.UsersDao;
import com.sadzeih.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.Date;

/**
 * Created by devos-f on 04/01/17.
 */

@RestController
public class YoutubeController {

    @Autowired
    private YoutubeAuth auth;

    @Bean
    public YoutubeAuth newYoutubeAuth() {
        return new YoutubeAuth();
    }

    @RequestMapping(path = "/createAction", method = RequestMethod.POST)
    public
    @ResponseBody
    Status createAction(@RequestParam(value = "userId") String userId,
                        @RequestParam(value = "name") String name,
                        @RequestParam(value = "video") String video) {
        YoutubeAction action = new YoutubeAction(name, video);
        DAOFactory.getInstance().getYoutubeActionDao().create(action, Long.valueOf(userId));
        return new Status("success", "Action created", null);
    }

    @RequestMapping(path = "/findAction", method = RequestMethod.POST)
    public
    @ResponseBody
    Status findAction(@RequestParam(value = "userId") String userId,
                      @RequestParam(value = "id") String id) {
        YoutubeAction action = DAOFactory.getInstance().getYoutubeActionDao().find(Long.valueOf(id), Long.valueOf(userId));
        return new Status("success", "Result action", action);
    }

    @RequestMapping(path = "/runAction/{id}/{userId}", method = RequestMethod.GET)
    public @ResponseBody Status runAction(@PathVariable int id,
                                          @PathVariable int userId)
    {
        try {
            ListenersActions.addWatchlist(DAOFactory.getInstance().getYoutubeActionDao().find((long)id, (long)userId));
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return new Status("success", "Mail sent", null);
    }

    @RequestMapping(path = "/createListener", method = RequestMethod.POST)
    public
    @ResponseBody
    Status createListener(@RequestParam(value = "userId") String userId,
                          @RequestParam(value = "name") String name) {
        YoutubeListener action = new YoutubeListener(name, null);
        DAOFactory.getInstance().getYoutubeListenerDao().create(action, Long.valueOf(userId));
        return new Status("success", "Action created", null);
    }

    @RequestMapping(path = "/findListener", method = RequestMethod.POST)
    public
    @ResponseBody
    Status findListener(@RequestParam(value = "userId") String userId,
                        @RequestParam(value = "id") String id) {
        YoutubeListener action = DAOFactory.getInstance().getYoutubeListenerDao().find(Long.valueOf(id), Long.valueOf(userId));
        return new Status("success", "Result listener", action);
    }


    @RequestMapping(path = "/callback", method = RequestMethod.POST)
    public
    @ResponseBody
    Status callback(@RequestParam(value="code") String code,
                    @RequestParam(value="state") String state) {

        try {
            auth.setCode(code);
            auth.postCode();
            DAOFactory factory = DAOFactory.getInstance();
            AccessTokenDao tokenRepo = factory.getAccessTokenDao();

            AccessToken userToken = tokenRepo.findByToken(state);
            UsersDao userRepo = factory.getUsersDao();
            Users user = userRepo.findById(userToken.getUserId());

            AccessToken findToken = tokenRepo.find(user.getId(), "youtube");
            if (findToken != null) {
                tokenRepo.remove(findToken.getId());
            }
            AccessToken newToken = new AccessToken();
            newToken.setToken(auth.getToken());
            newToken.setUserId(user.getId());
            newToken.setType("youtube");
            tokenRepo.create(newToken);

            AccessToken refreshToken = new AccessToken();
            refreshToken.setToken(auth.getRefreshToken());
            refreshToken.setUserId(user.getId());
            refreshToken.setType("youtubeRefresh");
            tokenRepo.create(refreshToken);

            return (new Status("success", "Youtube authentication", null));
        } catch (Exception e){
            return (new Status("error", "Youtube authentication failed", null));
        }
    }

    @RequestMapping(path = "/auth", method = RequestMethod.POST)
    public Status authenticate(@RequestParam(value = "token") String token) {
        return new Status("info", "Authorize URL", auth.buildAuthorizeURL(token));
    }
}
