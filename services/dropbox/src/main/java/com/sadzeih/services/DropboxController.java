package com.sadzeih.services;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.users.FullAccount;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sadzeih.beans.*;
import com.sadzeih.dao.AccessTokenDao;
import com.sadzeih.dao.ActionDao;
import com.sadzeih.dao.DropboxListenerDao;
import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.dao.UsersDao;
import com.sadzeih.utils.Status;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.http.client.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import javax.ws.rs.Path;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by devos-f on 04/01/17.
 */

@RestController
public class DropboxController {

    @Autowired
    private DropboxAuth auth;

    @Autowired
    public RestTemplate restTemplate;

    @Bean
    public DropboxAuth newDropboxAuth() {
        return new DropboxAuth();
    }

    @RequestMapping(path = "/callback/{code}/{state}", method = RequestMethod.GET)
    public
    @ResponseBody
    Status callback(@PathVariable String code,
                    @PathVariable String state) throws DbxException {

            auth.setCode(code);
            auth.postCode();
            DAOFactory factory = DAOFactory.getInstance();
            AccessTokenDao tokenRepo = factory.getAccessTokenDao();

            AccessToken userToken = tokenRepo.findByToken(state);
            UsersDao userRepo = factory.getUsersDao();
            Users user = userRepo.findById(userToken.getUserId());

            AccessToken findToken = tokenRepo.find(user.getId(), "Dropbox");
            if (findToken != null && findToken.getExpirationDate().getTime() <= new Date().getTime()) {
                tokenRepo.remove(findToken.getId());
            }
            AccessToken newToken = new AccessToken();
            newToken.setToken(auth.getToken());
            newToken.setUserId(user.getId());
            newToken.setType("Dropbox");

            DbxRequestConfig config = new DbxRequestConfig("API");
            DbxClientV2 client = new DbxClientV2(config, auth.getToken());
            FullAccount account = client.users().getCurrentAccount();

            System.out.println(account.getAccountId());

            newToken.setAccountId(account.getAccountId());
            tokenRepo.create(newToken);

            return (new Status("success", "Dropbox authentication", null));
    }

    @RequestMapping(path = "/createListener", method = RequestMethod.POST)
    public
    @ResponseBody
    Status createListener(@RequestParam(value = "userId") String userId,
                          @RequestParam(value = "name") String name) {
        DropboxListener action = new DropboxListener(name, null);
        DAOFactory.getInstance().getDropboxListenerDao().create(action, Long.valueOf(userId));
        return new Status("success", "Listener created", null);
    }

    @RequestMapping(path = "/createAction", method = RequestMethod.POST)
    public
    @ResponseBody
    Status createAction(@RequestParam(value = "userId") String userId,
                        @RequestParam(value = "name") String name,
                        @RequestParam(value = "fileName") String fileName) {
        DropboxAction action = new DropboxAction(name, fileName);
        DAOFactory.getInstance().getDropboxActionDao().create(action, Long.valueOf(userId));
        return new Status("success", "Action created", null);
    }

    @RequestMapping(path = "/runAction/{id}/{userId}")
    public Status runAction(@PathVariable String id, @PathVariable String userId) throws DbxException {
        // Get the AccessToken (for the dropbox token)
        AccessToken token = DAOFactory.getInstance().getAccessTokenDao().find(Long.valueOf(userId), "dropbox");
        // Get the DropboxAction (for the file name)
        DropboxAction action = DAOFactory.getInstance().getDropboxActionDao().find(Long.valueOf(id), Long.valueOf(userId));
        DbxRequestConfig config = new DbxRequestConfig("API");
        DbxClientV2 client = new DbxClientV2(config, token.getAccountId());
        client.files().delete(action.getFileName());
        return new Status("success", "file deletion", null);
    }

    @RequestMapping(path = "/webhook", method = RequestMethod.POST)
    public Status webhook(@RequestBody String json) {
        // Parsing JSON to get accountId
        JsonElement jelement = new JsonParser().parse(json);
        JsonObject  jobject = jelement.getAsJsonObject();
        jobject = jobject.getAsJsonObject("list_folder");
        JsonArray jarray = jobject.getAsJsonArray("accounts");
        String accountId;
        for (int k = 0; k < jarray.size(); ++k) {
            accountId = jarray.get(k).getAsString();
            // Get userId matching accountId
            AccessTokenDao tokenDao = DAOFactory.getInstance().getAccessTokenDao();
            AccessToken token = tokenDao.findByAccountId(accountId);

            if (token == null) {
                return new Status("error", "Cannot find accountId", null);
            }
            // Get each dropboxListener matching userId
            DropboxListenerDao listenerDao = DAOFactory.getInstance().getDropboxListenerDao();
            List<DropboxListener> dropboxListener = listenerDao.findByUserId(token.getUserId());

            for (int i = 0; i < dropboxListener.size(); ++i) {

                // Get each action matching dropboxListener.id
                ActionDao actionDao = DAOFactory.getInstance().getActionDao();
                List<Action> actions = actionDao.findByListener(token.getUserId(), dropboxListener.get(i).getId());

                // Call each action.type with action.id
                for (int j = 0; j < actions.size(); ++j) {
                    HttpHeaders headers = new HttpHeaders();
                    headers.setContentType(MediaType.MULTIPART_FORM_DATA);
                    MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
                    map.add("id", actions.get(j).getId().toString());
                    HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
                    String url;
                    if (actions.get(j).getType() == "GMailAction") {
                        url = "http://gmail-service/runAction/" + actions.get(i).getId() + "/" + actions.get(i).getUserId();
                    } else if (actions.get(j).getType() == "DropboxAction") {
                        url = "http://dropbox-service/runAction/" + actions.get(i).getId() + "/" + actions.get(i).getUserId();
                    } else if (actions.get(j).getType() == "YoutubeAction") {
                        url = "http://youtube-service/runAction/" + actions.get(i).getId() + "/" + actions.get(i).getUserId();
                    } else {
                        continue;
                    }
                    restTemplate.postForObject(url, request, String.class, new HashMap<>());
                }
            }
        }

        return new Status("success", "Actions launched", null);
    }

    @RequestMapping(path = "/auth", method = RequestMethod.POST)
    public Status authenticate(@RequestParam(value = "token") String token) {
        return new Status("info", "Authorize URL", auth.buildAuthorizeURL(token));
    }
}
