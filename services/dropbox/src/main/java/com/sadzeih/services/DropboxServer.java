package com.sadzeih.services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;

@EnableAutoConfiguration
@EnableDiscoveryClient
@Import(DropboxService.class)
public class DropboxServer {
	public static void main(String[] args) {
		System.setProperty("spring.config.name", "dropbox-server");

		SpringApplication.run(DropboxServer.class, args);
	}
}
