package com.sadzeih.beans;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by desrum_m on 01/02/17.
 */
public class DropboxListener extends Listener {
    public DropboxListener(String name, List<Action> links) {
        super(null, "DropboxListener", name, links);
    }

    public DropboxListener() {
    }
}
