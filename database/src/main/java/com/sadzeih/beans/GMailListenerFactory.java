package com.sadzeih.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by desrum_m on 04/02/17.
 */
public class GMailListenerFactory implements ListenerFactory {
    public GMailListener create(ResultSet resultSet) throws SQLException {
        GMailListener listener = new GMailListener();
        listener.fromResultSet(resultSet);
        return listener;
    }
}
