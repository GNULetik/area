package com.sadzeih.beans;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Action {
    Long            id;
    Long            userId;
    String          type;
    String          name;
    List<Listener>  links;

    public Action() {}

    public Action(Long id, String type, String name, List<Listener> links) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.links = links;
    }

    public Action(ResultSet resultSet) throws SQLException {
        this.fromResultSet(resultSet);
    }

    public void fromResultSet(ResultSet resultSet) throws SQLException {
        this.id = resultSet.getLong("actionId");
        this.userId = resultSet.getLong("actionUserId");
        this.type = resultSet.getString("actionType");
        this.name = resultSet.getString("actionName");
        this.links = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Listener> getLinks() {
        return links;
    }

    public void setLinks(List<Listener> links) {
        this.links = links;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}