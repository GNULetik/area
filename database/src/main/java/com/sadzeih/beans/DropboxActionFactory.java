package com.sadzeih.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by desrum_m on 04/02/17.
 */
public class DropboxActionFactory implements ActionFactory {
    public DropboxAction create(ResultSet resultSet) throws SQLException {
        DropboxAction listener = new DropboxAction();
        listener.fromResultSet(resultSet);
        return listener;
    }
}
