package com.sadzeih.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by desrum_m on 04/02/17.
 */
public interface ListenerFactory {
    public <T extends Listener> T create(ResultSet resultset) throws SQLException;
}
