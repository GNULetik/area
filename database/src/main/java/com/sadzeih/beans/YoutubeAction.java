package com.sadzeih.beans;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class YoutubeAction extends Action {
    private String  video;

    public YoutubeAction() {}

    // This constructor aims to be used to add a new Action to the DB
    public YoutubeAction(String name, String video) {
        super(null, "youtube", name, null);
        this.video = video;
    }

    public YoutubeAction(Long id, String type, String name, List<Listener> links, String video) {
        super(id, type, name, links);
        this.video = video;
    }

    public void fromResultSet(ResultSet resultSet) throws SQLException {
        super.fromResultSet(resultSet);
        this.video = resultSet.getString("actionVideo");
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }
}