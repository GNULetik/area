package com.sadzeih.beans;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by desrum_m on 01/02/17.
 */
public class GMailListener extends Listener {
    String labelName;
    String topicName;

    public GMailListener() {}

    // This constructor aims to be used to add a new Listener to the DB
    public GMailListener(String name, List<Action> links, String labelName, String topicName) {
        super(null, "gmail", name, links);
        this.labelName = labelName;
        this.topicName = topicName;
    }

    public GMailListener(Long id, String type, String name, List<Action> links, String labelName, String topicName) {
        super(id, type, name, links);
        this.labelName = labelName;
        this.topicName = topicName;
    }

    public void fromResultSet(ResultSet resultSet) throws SQLException {
        super.fromResultSet(resultSet);
        this.labelName = resultSet.getString("listenerLabelName");
        this.topicName = resultSet.getString("listenerTopicName");
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }
}
