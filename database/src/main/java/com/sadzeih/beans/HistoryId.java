package com.sadzeih.beans;

import java.math.BigInteger;

/**
 * Created by sadzeih on 2/5/17.
 */
public class HistoryId {
    private Long id;
    private BigInteger historyId;
    private Long userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public BigInteger getHistoryId() {
        return historyId;
    }

    public void setHistoryId(BigInteger historyId) {
        this.historyId = historyId;
    }
}
