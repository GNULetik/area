package com.sadzeih.beans;

/**
 * Created by sadzeih on 2/6/17.
 */
public class YoutubeLikes {
    private Long id;
    private Long userId;
    private Long likes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getLikes() {
        return likes;
    }

    public void setLikes(Long likes) {
        this.likes = likes;
    }
}
