package com.sadzeih.beans;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class GMailAction extends Action {
    String  destinationAddress;
    String  subject;
    String  body;

    public GMailAction() {}

    // This constructor aims to be used to add a new Action to the DB
    public GMailAction(String name, String destinationAddress, String subject, String body) {
        super(null, "gmail", name, null);
        this.destinationAddress = destinationAddress;
        this.subject = subject;
        this.body = body;
    }

    public GMailAction(Long id, String type, String name, List<Listener> links, String destinationAddress, String subject, String body) {
        super(id, type, name, links);
        this.destinationAddress = destinationAddress;
        this.subject = subject;
        this.body = body;
    }

    public void fromResultSet(ResultSet resultSet) throws SQLException {
        super.fromResultSet(resultSet);
        this.destinationAddress = resultSet.getString("actionDestinationAddress");
        this.subject = resultSet.getString("actionSubject");
        this.body = resultSet.getString("actionBody");
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}