package com.sadzeih.beans;

/**
 * Created by devos-f on 24/01/17.
 */
public class OAuthToken {

    private Long id;
    private String externToken;
    private String localToken;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExternToken() {
        return externToken;
    }

    public void setExternToken(String externToken) {
        this.externToken = externToken;
    }

    public String getLocalToken() {
        return localToken;
    }

    public void setLocalToken(String localToken) {
        this.localToken = localToken;
    }
}
