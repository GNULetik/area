package com.sadzeih.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by desrum_m on 04/02/17.
 */
public class YoutubeListenerFactory implements ListenerFactory {
    public YoutubeListener create(ResultSet resultSet) throws SQLException {
        YoutubeListener listener = new YoutubeListener();
        listener.fromResultSet(resultSet);
        return listener;
    }
}
