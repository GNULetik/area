package com.sadzeih.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by desrum_m on 04/02/17.
 */
public class DropboxListenerFactory implements ListenerFactory {
    public DropboxListener create(ResultSet resultSet) throws SQLException {
        DropboxListener listener = new DropboxListener();
        listener.fromResultSet(resultSet);
        return listener;
    }
}
