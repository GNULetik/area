package com.sadzeih.beans;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by desrum_m on 01/02/17.
 */
public class YoutubeListener extends Listener {

    public YoutubeListener() {}

    // This constructor aims to be used to add a new Listener to the DB
    public YoutubeListener(String name, List<Action> links) {
        super(null, "youtube", name, links);
    }

    public YoutubeListener(Long id, String type, String name, List<Action> links, String labelName, String topicName) {
        super(id, type, name, links);
    }

    public void fromResultSet(ResultSet resultSet) throws SQLException {
        super.fromResultSet(resultSet);
    }
}
