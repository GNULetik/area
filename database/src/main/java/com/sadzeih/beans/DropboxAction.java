package com.sadzeih.beans;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class DropboxAction extends Action {
    String fileName;

    public DropboxAction() {}

    // This constructor aims to be used to add a new Action to the DB
    public DropboxAction(String name, String fileName) {
        super(null, "gmail", name, null);
        this.fileName = fileName;
    }

    public DropboxAction(Long id, String type, String name, List<Listener> links, String fileName) {
        super(id, type, name, links);
        this.fileName = fileName;
    }

    public void fromResultSet(ResultSet resultSet) throws SQLException {
        super.fromResultSet(resultSet);
        this.fileName = resultSet.getString("actionFileName");
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}