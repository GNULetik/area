package com.sadzeih.beans;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Listener {
    Long            id;
    Long            userId;
    String          type;
    String          name;
    List<Action>    links;

    public Listener() {}

    public Listener(Long id, String type, String name, List<Action> links) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.links = links;
    }

    public Listener(ResultSet resultSet) throws SQLException {
        this.fromResultSet(resultSet);
    }

    public void fromResultSet(ResultSet resultSet) throws SQLException {
        this.id = resultSet.getLong("listenerId");
        this.userId = resultSet.getLong("listenerUserId");
        this.type = resultSet.getString("listenerType");
        this.name = resultSet.getString("listenerName");
        this.links = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Action> getLinks() {
        return links;
    }

    public void setLinks(List<Action> links) {
        this.links = links;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}