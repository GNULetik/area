package com.sadzeih.daoHelper;

/**
 * Created by devos-f on 26/12/16.
 */
public class DAOException extends RuntimeException{

    public DAOException(String message) {
        super(message);
    }

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    public DAOException(Throwable cause) {
        super(cause);
    }

}
