package com.sadzeih.daoHelper;

import java.sql.*;

/**
 * Created by devos-f on 28/12/16.
 */
public final class DAOUtils {

    private DAOUtils() {
    }

    public static PreparedStatement initPreparedRequest(Connection connection, String sql, boolean returnGeneratedKeys, Object... objects) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(sql, returnGeneratedKeys ? Statement.RETURN_GENERATED_KEYS : Statement.NO_GENERATED_KEYS);
        for (int i = 0; i < objects.length; i++){
            preparedStatement.setObject(i+ 1, objects[i]);
        }

        return preparedStatement;
    }

    public static void silentShutdown( ResultSet resultSet ) {
        if ( resultSet != null ) {
            try {
                resultSet.close();
            } catch ( SQLException e ) {
                System.out.println( "Échec de la fermeture du ResultSet : " + e.getMessage() );
            }
        }
    }

    public static void silentShutdown( Statement statement ) {
        if ( statement != null ) {
            try {
                statement.close();
            } catch ( SQLException e ) {
                System.out.println( "Échec de la fermeture du Statement : " + e.getMessage() );
            }
        }
    }

    public static void silentShutdown( Connection connexion ) {
        if ( connexion != null ) {
            try {
                connexion.close();
            } catch ( SQLException e ) {
                System.out.println( "Échec de la fermeture de la connexion : " + e.getMessage() );
            }
        }
    }

    public static void silentShutdown( Statement statement, Connection connexion ) {
        silentShutdown( statement );
        silentShutdown( connexion );
    }

    public static void silentShutdown( ResultSet resultSet, Connection connection) {
        silentShutdown(resultSet);
        silentShutdown(connection);
    }

    public static void silentShutdown(ResultSet resultSet, Statement statement, Connection connexion ) {
        silentShutdown( resultSet );
        silentShutdown( statement );
        silentShutdown( connexion );
    }


}
