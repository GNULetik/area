package com.sadzeih.daoHelper;

import com.sadzeih.beans.DropboxListener;
import com.sadzeih.beans.HistoryId;
import com.sadzeih.beans.Listener;
import com.sadzeih.dao.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by devos-f on 28/12/16.
 */
public class DAOFactory {

    private static final String PROPERTY_URL = "jdbc:mysql://localhost:3306/area";
    private static final String PROPERTY_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String PROPERTY_USER = "root";
    private static final String PROPERTY_PASSWORD = "";

    private String url;
    private String username;
    private String password;

    DAOFactory(String url, String username, String password){
        this.url = url;
        this.username = username;
        this.password = password;
    }

     /*
     * Méthode chargée de récupérer les informations de connexion à la base de
     * données, charger le driver JDBC et retourner une instance de la Factory
     */

    public static DAOFactory getInstance() throws DAOConfigurationException {
        String url;
        String driver;
        String user;
        String password;

        url = PROPERTY_URL;
        driver = PROPERTY_DRIVER;
        user = PROPERTY_USER;
        password = PROPERTY_PASSWORD;

        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            throw new DAOConfigurationException("Driver not found in " + e);
        }

        DAOFactory instance = new DAOFactory(url, user, password);
        return (instance);
    }

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, username, password);
    }

    public UsersDao getUsersDao(){
        return new UsersDaoImpl(this);
    }

    public AccessTokenDao getAccessTokenDao() {
        return new AccessTokenDaoImpl(this);
    }

    public OAuthTokenDao getOAuthTokenDao() {
        return new OAuthTokenDaoImpl(this);
    }

    public ListenerDao getListenerDao() { return new ListenerDaoImpl(this); }

    public ActionDao getActionDao() {
        return new ActionDaoImpl(this);
    }

    public GMailListenerDao getGMailListenerDao() { return new GMailListenerDaoImpl(this); }

    public GMailActionDao getGMailActionDao() { return new GMailActionDaoImpl(this); }

    public HistoryIdDao getHistoryIdDao() { return new HistoryIdDaoImpl(this); }

    public DropboxListenerDao getDropboxListenerDao() { return new DropboxListenerDaoImpl(this); }

    public DropboxActionDao getDropboxActionDao() { return new DropboxActionDaoImpl(this); }

    public YoutubeListenerDao getYoutubeListenerDao() {
        return new YoutubeListenerDaoImpl(this);
    }

    public YoutubeActionDao getYoutubeActionDao() {
        return new YoutubeActionDaoImpl(this);
    }

    public YoutubeLikesDao getYoutubeLikesDao() {
        return new YoutubeLikesDaoImpl(this);
    }
}
