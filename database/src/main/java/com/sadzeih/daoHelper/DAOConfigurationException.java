package com.sadzeih.daoHelper;

/**
 * Created by devos-f on 26/12/16.
 */
public class DAOConfigurationException extends RuntimeException {

    public DAOConfigurationException (String message){
        super(message);
    }

    public DAOConfigurationException (String message, Throwable cause){
        super(message, cause);
    }

    public DAOConfigurationException (Throwable cause){
        super (cause);
    }
}
