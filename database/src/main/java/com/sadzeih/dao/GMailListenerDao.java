package com.sadzeih.dao;

import com.sadzeih.beans.GMailAction;
import com.sadzeih.beans.GMailListener;
import com.sadzeih.daoHelper.DAOException;

import java.util.List;

public interface GMailListenerDao {
    // This function do not add links because they are already added when creating the listener
    void create(GMailListener action, Long userId) throws DAOException;
    GMailListener find(Long id, Long userId) throws DAOException;
    List<GMailListener> findAll() throws DAOException;
    void remove (Long id, Long userId) throws DAOException;
}
