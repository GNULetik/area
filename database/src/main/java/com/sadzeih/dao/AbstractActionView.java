package com.sadzeih.dao;

import com.sadzeih.beans.Action;
import com.sadzeih.beans.Listener;
import com.sadzeih.daoHelper.DAOException;
import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.daoHelper.DAOUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

/**
 * Created by desrum_m on 01/02/17.
 */
public abstract class AbstractActionView {
    protected DAOFactory daoFactory;
    protected String findQuery;
    private String removeQuery;
    private String removeAllJoinQuery;

    AbstractActionView(DAOFactory daoFactory) {
        this.findQuery = "SELECT l.id AS listenerId, l.type AS listenerType, l.name AS listenerName, " +
                "a.id AS actionId, a.type AS actionType, a.name AS actionName, a.userId AS actionUserId " +
                "FROM Listener l " +
                "INNER JOIN ListenerAction la " +
                "ON la.listenerId = l.id " +
                "INNER JOIN Action a " +
                "ON a.id = la.actionId " +
                "WHERE a.userId = ? " +
                "AND l.userId = a.userId " +
                "AND a.id = ?";
        this.removeQuery = "DELETE FROM Action WHERE userId = ? AND WHERE id = ?";
        this.removeAllJoinQuery = "DELETE la FROM ListenerAction la " +
                "INNER JOIN Listener l " +
                "ON la.listenerId = l.id " +
                "INNER JOIN Action a " +
                "ON la.actionId = a.id " +
                "WHERE a.userId = ? " +
                "AND l.userId = a.userId " +
                "AND actionId = ?";
        this.daoFactory = daoFactory;
    }

    protected <T extends Action> T find(Long id, Long userId, T action) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, this.findQuery, false, id, userId);
            System.out.println(preparedStatement);
            resultSet = preparedStatement.executeQuery();
            /*if (resultSet.getFetchSize() == 0) {
                return null;
            }*/
            while (resultSet.next()) {
                if (resultSet.isFirst()) {
                    action.fromResultSet(resultSet);
                }
                if (!isListenerInAction(action, resultSet)) {
                    action.getLinks().add(mapLinkListener(resultSet));
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement, connection);
        }
        return (action);
    }

    public void remove(Long id, Long userId) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, this.removeQuery, false, userId, id);
            int status = preparedStatement.executeUpdate();
            if (status == 0){
                throw new DAOException("Access token remove failed in database");
            }
            preparedStatement = DAOUtils.initPreparedRequest(connection, this.removeAllJoinQuery, false, userId, id);
            status = preparedStatement.executeUpdate();
            if (status == 0){
                throw new DAOException("Access token remove failed in database");
            }
        } catch (SQLException e){
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(preparedStatement,connection);
        }
    }

    protected boolean isListenerInAction(Action action, ResultSet resultSet) throws SQLException {
        Long actionId = (long) resultSet.getInt("listenerId");
        for (int i = 0; i < action.getLinks().size(); ++i) {
            Listener tmp = action.getLinks().get(i);
            if (Objects.equals(tmp.getId(), actionId)) {
                return true;
            }
        }
        return false;
    }

    protected Listener mapLinkListener(ResultSet resultSet) throws SQLException {
        return new Listener(
                resultSet.getLong("listenerId"),
                resultSet.getString("listenerType"),
                resultSet.getString("listenerName"),
                null
        );
    }
}
