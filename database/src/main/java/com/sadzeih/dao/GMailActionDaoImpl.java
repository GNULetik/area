package com.sadzeih.dao;

import com.sadzeih.beans.GMailAction;
import com.sadzeih.daoHelper.DAOException;
import com.sadzeih.daoHelper.DAOFactory;

/**
 * Created by devos-f on 02/01/17.
 */
public class GMailActionDaoImpl extends AbstractAction implements GMailActionDao {
    public GMailActionDaoImpl(DAOFactory daoFactory){
        super(daoFactory);
        this.createQuery = "INSERT INTO GMailAction " +
            "(userId, type, name, destinationAddress, subject, body) " +
            "VALUES (?, ?, ?, ?, ?, ?)";
        this.findQuery = "SELECT l.id AS listenerId, l.type AS listenerType, l.name AS listenerName, " +
                "a.id AS actionId, a.type AS actionType, a.name AS actionName, a.userId AS actionUserId, " +
                "a.destinationAddress AS actionDestinationAddress, a.subject AS actionSubject, a.body AS actionBody " +
                "FROM Listener l " +
                "INNER JOIN ListenerAction la " +
                "ON la.listenerId = l.id " +
                "INNER JOIN GMailAction a " +
                "ON a.id = la.actionId " +
                "WHERE a.userId = ? " +
                "AND a.userId = l.userId " +
                "AND a.id = ?";
        this.daoFactory = daoFactory;
    }

    @Override
    public void create(GMailAction action, Long userId) throws DAOException {
        super.create(action, userId, action.getType(), action.getName(),
                    action.getDestinationAddress(), action.getSubject(), action.getBody());
    }

    @Override
    public GMailAction find(Long id, Long userId) throws DAOException {
        return super.find(id, userId, new GMailAction());
    }
}
