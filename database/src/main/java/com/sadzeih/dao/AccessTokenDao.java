package com.sadzeih.dao;

import com.sadzeih.beans.AccessToken;
import com.sadzeih.daoHelper.DAOException;

import java.util.List;

/**
 * Created by devos-f on 02/01/17.
 */
public interface AccessTokenDao {

    void create (AccessToken accessToken) throws DAOException;

    AccessToken find (Long userId, String type) throws DAOException;

    void remove (Long id) throws  DAOException;

    List<AccessToken> findAll(Long id) throws DAOException;

    AccessToken findByToken(String token) throws DAOException;

    AccessToken findByAccountId(String accountId) throws DAOException;

}
