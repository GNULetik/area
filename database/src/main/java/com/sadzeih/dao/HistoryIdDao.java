package com.sadzeih.dao;

import com.sadzeih.beans.HistoryId;
import com.sadzeih.daoHelper.DAOException;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by sadzeih on 2/5/17.
 */
public interface HistoryIdDao {

    void create (HistoryId id) throws DAOException;

    HistoryId find (Long userId) throws DAOException;

    void remove (Long id) throws DAOException;

}
