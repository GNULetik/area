package com.sadzeih.dao;

import com.sadzeih.beans.Users;
import com.sadzeih.daoHelper.DAOException;

/**
 * Created by devos-f on 28/12/16.
 */
public interface UsersDao {

    void create(Users users) throws DAOException;

    Users findByMail(String email) throws DAOException;

    Users findById(Long id) throws DAOException;

    void remove(Long id) throws DAOException;

}
