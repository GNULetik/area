package com.sadzeih.dao;

import com.sadzeih.beans.Listener;
import com.sadzeih.daoHelper.DAOException;

import java.util.List;

public interface ListenerDao {
    List<Listener> getAll (Long userId) throws DAOException;
    Listener find(Long id, Long userId) throws DAOException;
    void addAction (Long listenerId, Long actionId, Long userId) throws DAOException;
    void removeAction (Long listenerId, Long actionId, Long userId) throws DAOException;
    void remove (Long id, Long userId) throws DAOException;
}
