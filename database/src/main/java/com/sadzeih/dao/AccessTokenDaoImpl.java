package com.sadzeih.dao;

import com.sadzeih.beans.AccessToken;
import com.sadzeih.daoHelper.DAOException;
import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.daoHelper.DAOUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by devos-f on 02/01/17.
 */
public class AccessTokenDaoImpl implements AccessTokenDao {

    private DAOFactory daoFactory;
    private static final String CREATE_TOKEN = "INSERT INTO AccessToken (userId, type, token, generationDate, expirationDate, accountId) VALUES (?, ?, ?, NOW(), NOW() + INTERVAL 60 DAY, ?)";
    private static final String FIND_TOKEN = "SELECT * FROM AccessToken WHERE userId = ? AND type = ?";
    private static final String REMOVE_TOKEN = "DELETE FROM AccessToken WHERE id = ?";
    private static final String FIND_ALL_TOKEN = "SELECT * FROM AccessToken WHERE userId = ?";
    private static final String FIND_BY_TOKEN = "SELECT * FROM AccessToken WHERE token = ?";
    private static final String FIND_BY_ACCOUNTID = "SELECT * FROM AccessToken WHERE accountId = ?";

    public AccessTokenDaoImpl(DAOFactory daoFactory){
        this.daoFactory = daoFactory;
    }

    @Override
    public void create(AccessToken accessToken) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, CREATE_TOKEN, true, accessToken.getUserId(), accessToken.getType(),
                    accessToken.getToken(), accessToken.getAccountId());
            int status = preparedStatement.executeUpdate();
            if (status == 0){
                throw new DAOException("User creation failed in database");
            }
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()){
                accessToken.setId(resultSet.getLong(1));
            } else {
                throw new DAOException("User creation failed, no auto generated ID returned");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement, connection);
        }
    }

    @Override
    public AccessToken find(Long userId, String type) throws DAOException, IllegalArgumentException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        AccessToken token = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, FIND_TOKEN, false, userId, type);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                token = map(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement, connection);
        }
        return (token);
    }

    @Override
    public void remove(Long id) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, REMOVE_TOKEN, false, id);
            int status = preparedStatement.executeUpdate();
            if (status == 0){
                throw new DAOException("Access token remove failed in database");
            }
        } catch (SQLException e){
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement,connection);
        }
    }

    @Override
    public List<AccessToken> findAll(Long id) throws DAOException, IllegalArgumentException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<AccessToken> result = new ArrayList<AccessToken>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, FIND_ALL_TOKEN, false, id);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                result.add(map(resultSet));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement, connection);
        }

        return (result);

    }

    @Override
    public AccessToken findByToken(String token) throws DAOException, IllegalArgumentException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        AccessToken result = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, FIND_BY_TOKEN, false, token);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                result = map(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement, connection);
        }
        return (result);

    }

    @Override
    public AccessToken findByAccountId(String accountId) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        AccessToken result = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, FIND_BY_TOKEN, false, accountId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                result = map(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement, connection);
        }
        return (result);
    }

    private static AccessToken map(ResultSet resultSet) throws SQLException {
        AccessToken token = new AccessToken();
        token.setId(resultSet.getLong("id"));
        token.setUserId(resultSet.getLong("userId"));
        token.setType(resultSet.getString("type"));
        token.setToken(resultSet.getString("token"));
        token.setGenerationDate(resultSet.getTimestamp("generationDate"));
        token.setExpirationDate(resultSet.getTimestamp("expirationDate"));
        token.setAccountId(resultSet.getString("accountId"));

        return token;
    }

}
