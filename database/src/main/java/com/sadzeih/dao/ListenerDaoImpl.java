package com.sadzeih.dao;

import com.sadzeih.beans.Listener;
import com.sadzeih.daoHelper.DAOException;
import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.daoHelper.DAOUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by devos-f on 02/01/17.
 */
public class ListenerDaoImpl extends AbstractListenerView implements ListenerDao {
    private String findAllQuery;

    public ListenerDaoImpl(DAOFactory daoFactory){
        super(daoFactory);
        this.findAllQuery = "SELECT l.id AS listenerId, l.type AS listenerType, l.name AS listenerName, l.userId AS listenerUserId " +
                "FROM Listener l " +
                "WHERE a.userId = ?;";
    }

    @Override
    public List<Listener> getAll(Long userId) throws DAOException {
        Connection connection = null;
        ResultSet resultSet = null;
        HashMap<Long, Listener> listenersMap = new HashMap<Long, Listener>();
        PreparedStatement preparedStatement;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, this.findAllQuery, false, userId);
            resultSet = preparedStatement.executeQuery();
            /*
            +------------+--------------+--------------+----------+------------+------------+
            | listenerId | listenerType | listenerName | actionId | actionType | actionName |
            +------------+--------------+--------------+----------+------------+------------+
            |          1 | typeTest     | nameTest     |        1 | typeTest   | nameTest   |
            +------------+--------------+--------------+----------+------------+------------+
             */
            while (resultSet.next()){
                Long id = (long) resultSet.getInt("listenerId");
                // If the listener doesn't already exist
                // We create it
                if (listenersMap.get(id) == null) {
                    listenersMap.put(id, new Listener(resultSet));
                }
                // If the Listener IS NOT already associated with the Action
                // (In the object, not the DB)
                // We add it to the object
                if (!isActionInListener(listenersMap.get(id), resultSet)) {
                    listenersMap.get(id).getLinks().add(mapLinkAction(resultSet));
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, connection);
        }
        List<Listener> actions;
        if (listenersMap != null) {
            actions = new ArrayList<>(listenersMap.values());
        } else {
            actions = new ArrayList<>();
        }
        return actions;
    }

    @Override
    public Listener find(Long id, Long userId) throws DAOException {
        return super.find(id, userId, new Listener());
    }
}
