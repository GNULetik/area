package com.sadzeih.dao;

import com.sadzeih.beans.Action;
import com.sadzeih.beans.ActionFactory;
import com.sadzeih.beans.Listener;
import com.sadzeih.beans.ListenerFactory;
import com.sadzeih.daoHelper.DAOException;
import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.daoHelper.DAOUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by desrum_m on 02/02/17.
 */
public abstract class AbstractAction extends AbstractActionView {
    protected String createQuery;
    protected String findByUserIdQuery;

    AbstractAction(DAOFactory daoFactory) {
        super(daoFactory);
    }

    // This function do not add links because they are already added when creating the listener
    protected <T extends Action> void create(T action, Object... objects) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, this.createQuery, true, objects);
            int status = preparedStatement.executeUpdate();
            if (status == 0){
                throw new DAOException("User creation failed in database");
            }
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()){
                action.setId(resultSet.getLong(1));
            } else {
                throw new DAOException("User creation failed, no auto generated ID returned");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement, connection);
        }
    }


    protected <T extends Action> List<T> findByUserId(ActionFactory factory, Long userId) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<T> listeners = new ArrayList<>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, this.findByUserIdQuery, false, userId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                listeners.add(factory.create(resultSet));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement, connection);
        }
        return (listeners);
    }
}
