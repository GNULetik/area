package com.sadzeih.dao;

import com.sadzeih.beans.*;
import com.sadzeih.daoHelper.DAOException;
import com.sadzeih.daoHelper.DAOFactory;

/**
 * Created by devos-f on 02/01/17.
 */
public class DropboxActionDaoImpl extends AbstractAction implements DropboxActionDao {
    public DropboxActionDaoImpl(DAOFactory daoFactory){
        super(daoFactory);
        this.createQuery = "INSERT INTO DropboxAction " +
            "(userId, type, name, fileName) " +
            "VALUES (?, ?, ?, ?)";
        this.findQuery = "SELECT l.id AS listenerId, l.type AS listenerType, l.name AS listenerName, " +
                "a.id AS actionId, a.type AS actionType, a.name AS actionName, a.userId AS actionUserId, " +
                "a.fileName AS actionFileName " +
                "FROM Listener l " +
                "INNER JOIN ListenerAction la " +
                "ON la.listenerId = l.id " +
                "INNER JOIN DropboxAction a " +
                "ON a.id = la.actionId " +
                "WHERE a.userId = ? " +
                "AND a.userId = l.userId " +
                "AND a.id = ?";
        this.findByUserIdQuery = "SELECT id AS listenerId, type AS listenerType, name AS listenerName, " +
                "userId as listenerUserId " +
                "FROM DropboxListener " +
                "WHERE userId = ?";
        this.daoFactory = daoFactory;
    }

    @Override
    public void create(DropboxAction action, Long userId) throws DAOException {
        super.create(action, userId, action.getType(), action.getName(),
                    action.getFileName());
    }

    @Override
    public DropboxAction find(Long id, Long userId) throws DAOException {
        return super.find(id, userId, new DropboxAction());
    }

}
