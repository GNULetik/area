package com.sadzeih.dao;

import com.sadzeih.beans.Action;
import com.sadzeih.beans.Listener;
import com.sadzeih.beans.ListenerFactory;
import com.sadzeih.daoHelper.DAOException;
import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.daoHelper.DAOUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by desrum_m on 02/02/17.
 */
public abstract class AbstractListener extends AbstractListenerView {
    protected String createQuery;
    protected String findAllQuery;
    protected String findByUserIdQuery;

    AbstractListener(DAOFactory daoFactory) {
        super(daoFactory);
    }
    protected <T extends Listener> void create(T listener, Object... objects) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, this.createQuery, true,
                    objects
            );
            int status = preparedStatement.executeUpdate();
            if (status == 0){
                throw new DAOException("User creation failed in database");
            }
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()){
                listener.setId(resultSet.getLong(1));
            } else {
                throw new DAOException("User creation failed, no auto generated ID returned");
            }
            if (listener.getLinks() != null && listener.getLinks().size() != 0) {
                // TODO: Can be optimised to insert every Join at the same time
                for (int i = 0; i < listener.getLinks().size(); ++i ) {
                    Action action = listener.getLinks().get(i);
                    preparedStatement = DAOUtils.initPreparedRequest(connection, this.createJoinQuery, false,
                            action.getId(), listener.getId()
                    );
                    status = preparedStatement.executeUpdate();
                    if (status == 0){
                        throw new DAOException("User creation failed in database");
                    }
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement, connection);
        }
    }

    protected <T extends Listener> List<T> findAll(ListenerFactory factory) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<T> listeners = new ArrayList<>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, this.findAllQuery, false);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                listeners.add(factory.create(resultSet));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement, connection);
        }
        return (listeners);
    }

    protected <T extends Listener> List<T> findByUserId(ListenerFactory factory, Long userId) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<T> listeners = new ArrayList<>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, this.findByUserIdQuery, false, userId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                listeners.add(factory.create(resultSet));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement, connection);
        }
        return (listeners);
    }
}
