package com.sadzeih.dao;

import com.sadzeih.beans.OAuthToken;
import com.sadzeih.daoHelper.DAOException;
import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.daoHelper.DAOUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by devos-f on 24/01/17.
 */
public class OAuthTokenDaoImpl implements OAuthTokenDao {
    private DAOFactory daoFactory;
    private static final String CREATE_TOKEN = "INSERT INTO OAuthToken (id, externToken, localToken) VALUES (?,?,?);";
    private static final String FIND_TOKEN = "SELECT * FROM OAuthToken WHERE externToken = ?";
    private static final String REMOVE_TOKEN = "DELETE FROM OAuthToken WHERE id = ?";
    private static final String FIND_ALL_TOKEN = "SELECT * FROM AccessToken";

    public OAuthTokenDaoImpl(DAOFactory daoFactory){
        this.daoFactory = daoFactory;
    }

    @Override
    public void create(OAuthToken token) throws DAOException, IllegalArgumentException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, CREATE_TOKEN, true, token.getId(), token.getExternToken(),
                    token.getLocalToken());
            int status = preparedStatement.executeUpdate();
            if (status == 0){
                throw new DAOException("User creation failed in database");
            }
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()){
                token.setId(resultSet.getLong(1));
            } else {
                throw new DAOException("User creation failed, no auto generated ID returned");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement, connection);
        }

    }

    @Override
    public OAuthToken find(String externToken) throws DAOException, IllegalArgumentException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        OAuthToken token = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, FIND_TOKEN, false, externToken);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                token = map(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement, connection);
        }
        return (token);

    }

    @Override
    public List<OAuthToken> findAll() throws DAOException, IllegalArgumentException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<OAuthToken> result = new ArrayList<>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, FIND_ALL_TOKEN, false);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                result.add(map(resultSet));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement, connection);
        }

        return (result);

    }

    @Override
    public void remove(Long id) throws DAOException, IllegalArgumentException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, REMOVE_TOKEN, false, id);
            int status = preparedStatement.executeUpdate();
            if (status == 0){
                throw new DAOException("Access token remove failed in database");
            }
        } catch (SQLException e){
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement,connection);
        }
    }

    private static OAuthToken map(ResultSet resultSet) throws SQLException {
        OAuthToken token = new OAuthToken();
        token.setId(resultSet.getLong("id"));
        token.setExternToken(resultSet.getString("externToken"));
        token.setLocalToken(resultSet.getString("localToken"));

        return token;
    }
}
