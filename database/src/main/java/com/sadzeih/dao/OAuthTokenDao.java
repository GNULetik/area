package com.sadzeih.dao;

import com.sadzeih.beans.OAuthToken;
import com.sadzeih.daoHelper.DAOException;

import java.util.List;

/**
 * Created by devos-f on 24/01/17.
 */
public interface OAuthTokenDao {

    void create (OAuthToken token) throws DAOException;

    OAuthToken find (String externalToken) throws DAOException;

    List<OAuthToken> findAll() throws DAOException;

    void remove (Long id) throws DAOException;
}
