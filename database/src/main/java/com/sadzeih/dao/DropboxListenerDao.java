package com.sadzeih.dao;

import com.sadzeih.beans.DropboxListener;
import com.sadzeih.beans.GMailListener;
import com.sadzeih.daoHelper.DAOException;

import java.util.List;

public interface DropboxListenerDao {
    // This function do not add links because they are already added when creating the listener
    void create(DropboxListener action, Long userId) throws DAOException;
    DropboxListener find(Long id, Long userId) throws DAOException;
    List<DropboxListener> findByUserId(Long userId);
    void remove(Long id, Long userId) throws DAOException;
}
