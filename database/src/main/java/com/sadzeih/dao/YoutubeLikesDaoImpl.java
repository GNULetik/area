package com.sadzeih.dao;

import com.sadzeih.beans.YoutubeLikes;
import com.sadzeih.daoHelper.DAOException;
import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.daoHelper.DAOUtils;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by sadzeih on 2/5/17.
 */
public class YoutubeLikesDaoImpl implements YoutubeLikesDao {
    private DAOFactory daoFactory;
    private final static String createQuery = "INSERT INTO YoutubeLikes (likes, userId) VALUES (?, ?);";
    private final static String findQuery = "SELECT * FROM YoutubeLikes WHERE userId = ?;";
    private final static String removeQuery = "DELETE FROM YoutubeLikes WHERE id = ?;";

    public YoutubeLikesDaoImpl(DAOFactory daoFactory){
        this.daoFactory = daoFactory;
    }

    public void create (YoutubeLikes id) throws DAOException
    {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, createQuery, true, id.getLikes(), id.getUserId());
            int status = preparedStatement.executeUpdate();
            if (status == 0){
                throw new DAOException("YoutubeLikes creation failed in database");
            }
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()){
                id.setId(resultSet.getLong(1));
            } else {
                throw new DAOException("YoutubeLikes creation failed, no auto generated ID returned");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement, connection);
        }
    }

    public YoutubeLikes find (Long userId) throws DAOException
    {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        YoutubeLikes id = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, findQuery, false, userId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                id = map(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement, connection);
        }
        return (id);
    }

    public void remove (Long id) throws DAOException
    {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, removeQuery, false, id);
            int status = preparedStatement.executeUpdate();
            if (status == 0){
                throw new DAOException("YoutubeLikes remove failed in database");
            }
        } catch (SQLException e){
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement,connection);
        }
    }

    private static YoutubeLikes map(ResultSet resultSet) throws SQLException {
        YoutubeLikes id = new YoutubeLikes();
        id.setId(resultSet.getLong("id"));
        id.setLikes(resultSet.getLong("likes"));
        id.setUserId(resultSet.getLong("userId"));
        return id;
    }
}
