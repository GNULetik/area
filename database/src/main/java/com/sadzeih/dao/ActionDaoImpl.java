package com.sadzeih.dao;

import com.sadzeih.beans.Action;
import com.sadzeih.daoHelper.DAOException;
import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.daoHelper.DAOUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by devos-f on 02/01/17.
 */
public class ActionDaoImpl extends AbstractActionView implements ActionDao {
    private String findAllQuery;
    private String findByListenerQuery;
    public ActionDaoImpl(DAOFactory daoFactory){
        super(daoFactory);
        this.findAllQuery = "SELECT l.id AS listenerId, l.type AS listenerType, l.name AS listenerName, " +
                "a.id AS actionId, a.type AS actionType, a.name AS actionName, a.userId AS actionUserId " +
                "FROM Listener l " +
                "INNER JOIN ListenerAction la " +
                "ON la.listenerId = l.id " +
                "INNER JOIN Action a " +
                "ON a.id = la.actionId " +
                "WHERE a.userId = l.userId " +
                "AND a.userId = ?";
        this.findByListenerQuery = "SELECT l.id AS listenerId, l.type AS listenerType, l.name AS listenerName, " +
                "a.id AS actionId, a.type AS actionType, a.name AS actionName, a.userId AS actionUserId " +
                "FROM Listener l " +
                "INNER JOIN ListenerAction la " +
                "ON la.listenerId = l.id " +
                "INNER JOIN Action a " +
                "ON a.id = la.actionId " +
                "WHERE a.userId = l.userId " +
                "AND a.userId = ? " +
                "AND l.id = ?";

    }

    @Override
    public List<Action> getAll(Long userId) throws DAOException {
        Connection connection = null;
        ResultSet resultSet = null;
        HashMap<Long, Action> actionsMap = new HashMap<Long, Action>();
        PreparedStatement preparedStatement;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, this.findAllQuery, false, userId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Long id = (long) resultSet.getInt("actionId");
                // If the Action doesn't already exist
                // We create it
                if (actionsMap.get(id) == null) {
                    actionsMap.put(id, new Action(resultSet));
                }
                // If the Action IS NOT already associated with the Listener
                // (In the object, not the DB)
                // We add it to the object
                if (!isListenerInAction(actionsMap.get(id), resultSet)) {
                    actionsMap.get(id).getLinks().add(mapLinkListener(resultSet));
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, connection);
        }
        List<Action> actions;
        if (actionsMap != null) {
            actions = new ArrayList<>(actionsMap.values());
        } else {
            actions = new ArrayList<>();
        }
        return actions;
    }

    public Action find(Long id, Long userId) throws DAOException {
        return this.find(id, userId, new Action());
    }

    public List<Action> findByListener(Long userId, Long listenerId) throws DAOException {
        Connection connection = null;
        ResultSet resultSet = null;
        HashMap<Long, Action> actionsMap = new HashMap<Long, Action>();
        PreparedStatement preparedStatement;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, this.findAllQuery, false, userId, listenerId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Long id = (long) resultSet.getInt("actionId");
                // If the Action doesn't already exist
                // We create it
                if (actionsMap.get(id) == null) {
                    actionsMap.put(id, new Action(resultSet));
                }
                // If the Action IS NOT already associated with the Listener
                // (In the object, not the DB)
                // We add it to the object
                if (!isListenerInAction(actionsMap.get(id), resultSet)) {
                    actionsMap.get(id).getLinks().add(mapLinkListener(resultSet));
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, connection);
        }
        List<Action> actions;
        if (actionsMap != null) {
            actions = new ArrayList<>(actionsMap.values());
        } else {
            actions = new ArrayList<>();
        }
        return actions;
    }
}
