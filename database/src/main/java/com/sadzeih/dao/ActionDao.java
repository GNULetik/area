package com.sadzeih.dao;

import com.sadzeih.beans.Action;
import com.sadzeih.daoHelper.DAOException;

import java.util.List;

/**
 * Created by devos-f on 02/01/17.
 */
public interface ActionDao {
    List<Action> getAll (Long userId) throws DAOException;
    Action find (Long id, Long userId) throws DAOException;
    List<Action> findByListener(Long userId, Long listenerId) throws DAOException;
    void remove (Long id, Long userId) throws DAOException;
}
