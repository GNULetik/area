package com.sadzeih.dao;

import com.sadzeih.beans.GMailAction;
import com.sadzeih.daoHelper.DAOException;

public interface GMailActionDao {
    void create(GMailAction action, Long userId) throws DAOException;
    GMailAction find(Long id, Long userId) throws DAOException;
    void remove (Long id, Long userId) throws DAOException;
}
