package com.sadzeih.dao;

import com.sadzeih.beans.YoutubeLikes;
import com.sadzeih.daoHelper.DAOException;

/**
 * Created by sadzeih on 2/5/17.
 */
public interface YoutubeLikesDao {

    void create(YoutubeLikes id) throws DAOException;

    YoutubeLikes find(Long userId) throws DAOException;

    void remove(Long id) throws DAOException;

}
