package com.sadzeih.dao;

import com.sadzeih.beans.Action;
import com.sadzeih.beans.Listener;
import com.sadzeih.daoHelper.DAOException;
import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.daoHelper.DAOUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

/**
 * Created by desrum_m on 02/02/17.
 */
public abstract class AbstractListenerView {
    protected DAOFactory daoFactory;
    protected String findQuery;
    protected String createJoinQuery;
    private String removeQuery;
    private String removeJoinQuery;
    private String removeAllJoinQuery;

    AbstractListenerView(DAOFactory daoFactory) {
        this.findQuery = "SELECT l.id AS listenerId, l.type AS listenerType, l.name AS listenerName, " +
                "a.id AS actionId, a.type AS actionType, a.name AS actionName " +
                "FROM Listener l " +
                "INNER JOIN ListenerAction la " +
                "ON la.listenerId = l.id " +
                "INNER JOIN Action a " +
                "ON a.id = la.actionId " +
                "WHERE a.userId = ? " +
                "AND a.userId = l.userId " +
                "AND l.id = ?";
        this.removeAllJoinQuery = "DELETE la FROM ListenerAction la " +
                "INNER JOIN Listener l " +
                "ON la.listenerId = l.id " +
                "INNER JOIN Action a " +
                "ON la.actionId = a.id " +
                "WHERE a.userId = ? " +
                "AND l.userId = a.userId " +
                "AND listenerId = ?";
        this.createJoinQuery = "INSERT INTO ListenerAction " +
            "(listenerId, actionId) " +
            "VALUES (?, ?)";
        this.removeJoinQuery = "DELETE la FROM ListenerAction la " +
                "INNER JOIN Listener l " +
                "ON la.listenerId = l.id " +
                "INNER JOIN Action a " +
                "ON la.actionId = a.id " +
                "WHERE a.userId = ? " +
                "AND l.userId = a.userId " +
                "AND listenerId = ? " +
                "AND actionId = ?";
        this.daoFactory = daoFactory;
    }

    protected <T extends Listener> T find(Long id, Long userId, T listener) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, this.findQuery, false, userId, id);
            System.out.println(preparedStatement);
            resultSet = preparedStatement.executeQuery();
            /*if (resultSet.getFetchSize() == 0) {
                return null;
            }*/
            while (resultSet.next()){
                if (resultSet.isFirst()) {
                    listener.fromResultSet(resultSet);
                }
                if (!isActionInListener(listener, resultSet)) {
                    listener.getLinks().add(mapLinkAction(resultSet));
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement, connection);
        }
        return (listener);
    }

    public void addAction(Long listenerId, Long actionId, Long userId) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, this.createJoinQuery, false,
                    listenerId, actionId
            );
            int status = preparedStatement.executeUpdate();
            if (status == 0){
                throw new DAOException("User creation failed in database");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(preparedStatement, connection);
        }
    }

    public void removeAction(Long listenerId, Long actionId, Long userId) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, this.removeJoinQuery, false,
                    userId, listenerId, actionId
            );
            int status = preparedStatement.executeUpdate();
            if (status == 0){
                throw new DAOException("User creation failed in database");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(preparedStatement, connection);
        }
    }

    public void remove(Long id, Long userId) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, this.removeQuery, false, userId, id);
            int status = preparedStatement.executeUpdate();
            if (status == 0){
                throw new DAOException("Access token remove failed in database");
            }
            preparedStatement = DAOUtils.initPreparedRequest(connection, this.removeAllJoinQuery, false, userId, id);
            status = preparedStatement.executeUpdate();
            if (status == 0){
                throw new DAOException("Access token remove failed in database");
            }
        } catch (SQLException e){
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(preparedStatement, connection);
        }
    }

    protected boolean isActionInListener(Listener listener, ResultSet resultSet) throws SQLException {
        Long actionId = (long) resultSet.getInt("actionId");
        for (int i = 0; i < listener.getLinks().size(); ++i) {
            Action tmp = listener.getLinks().get(i);
            if (Objects.equals(tmp.getId(), actionId)) {
                return true;
            }
        }
        return false;
    }

    protected Action mapLinkAction(ResultSet resultSet) throws SQLException {
        return new Action(
                resultSet.getLong("actionId"),
                resultSet.getString("actionType"),
                resultSet.getString("actionName"),
                null
        );
    }
}
