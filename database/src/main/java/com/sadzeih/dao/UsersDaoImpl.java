package com.sadzeih.dao;

import com.sadzeih.beans.Users;
import com.sadzeih.daoHelper.DAOException;
import com.sadzeih.daoHelper.DAOFactory;
import com.sadzeih.daoHelper.DAOUtils;

import java.sql.*;

/**
 * Created by devos-f on 28/12/16.
 */
public class UsersDaoImpl implements UsersDao {
    private DAOFactory daoFactory;
    private static final String SELECT_BY_MAIL = "SELECT id, email, nom, mot_de_passe, date_inscription FROM Users WHERE email = ?";
    private static final String INSERT_USER = "INSERT INTO Users (email, mot_de_passe, nom, date_inscription) VALUES (?, ?, 'test', NOW())";
    private static final String SELECT_BY_ID = "SELECT * FROM Users WHERE id = ?";
    private static final String REMOVE_USER = "DELETE FROM Users WHERE id = ?";

    public UsersDaoImpl(DAOFactory daoFactory){
        this.daoFactory = daoFactory;
    }

    @Override
    public void create(Users users) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, INSERT_USER, true, users.getEmail(), users.getPassword());
            int status = preparedStatement.executeUpdate();
            if (status == 0){
                throw new DAOException("User creation failed in database");
            }
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()){
                users.setId(resultSet.getLong(1));
            } else {
                throw new DAOException("User creation failed, no auto generated ID returned");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement, connection);
        }
    }

    @Override
    public void remove(Long id) throws IllegalArgumentException, DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, REMOVE_USER, false, id);
            int status = preparedStatement.executeUpdate();
            if (status == 0){
                throw new DAOException("User remove failed in database");
            }
        } catch (SQLException e){
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement,connection);
        }
    }

    @Override
    public Users findByMail(String email) throws IllegalArgumentException, DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Users user = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, SELECT_BY_MAIL, false, email);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                user = map(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement, connection);
        }

        return (user);
    }

    @Override
    public Users findById(Long id) throws IllegalArgumentException, DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Users user = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtils.initPreparedRequest(connection, SELECT_BY_ID, false, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                user = map(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.silentShutdown(resultSet, preparedStatement, connection);
        }

        return (user);

    }

    private static Users map(ResultSet resultSet) throws SQLException {
        Users users = new Users();
        users.setId(resultSet.getLong("id"));
        users.setEmail(resultSet.getString("email"));
        users.setPassword(resultSet.getString("mot_de_passe"));

        return users;
    }
}
