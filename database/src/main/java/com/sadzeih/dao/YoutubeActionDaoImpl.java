package com.sadzeih.dao;

import com.sadzeih.beans.GMailAction;
import com.sadzeih.beans.YoutubeAction;
import com.sadzeih.daoHelper.DAOException;
import com.sadzeih.daoHelper.DAOFactory;

/**
 * Created by devos-f on 02/01/17.
 */
public class YoutubeActionDaoImpl extends AbstractAction implements YoutubeActionDao {
    public YoutubeActionDaoImpl(DAOFactory daoFactory){
        super(daoFactory);
        this.createQuery = "INSERT INTO YoutubeAction " +
            "(userId, type, name, video) " +
            "VALUES (?, ?, ?, ?)";
        this.findQuery = "SELECT l.id AS listenerId, l.type AS listenerType, l.name AS listenerName, " +
                "a.id AS actionId, a.type AS actionType, a.name AS actionName, a.userId AS actionUserId, a.video as actionVideo " +
                "FROM Listener l " +
                "INNER JOIN ListenerAction la " +
                "ON la.listenerId = l.id " +
                "INNER JOIN YoutubeAction a " +
                "ON a.id = la.actionId " +
                "WHERE a.userId = ? " +
                "AND a.userId = l.userId " +
                "AND a.id = ?";
        this.daoFactory = daoFactory;
    }

    @Override
    public void create(YoutubeAction action, Long userId) throws DAOException {
        super.create(action, userId, action.getType(), action.getName(), action.getVideo());
    }

    @Override
    public YoutubeAction find(Long id, Long userId) throws DAOException {
        return super.find(id, userId, new YoutubeAction());
    }
}
