package com.sadzeih.dao;

import com.sadzeih.beans.GMailListener;
import com.sadzeih.beans.GMailListenerFactory;
import com.sadzeih.daoHelper.DAOException;
import com.sadzeih.daoHelper.DAOFactory;

import java.util.List;

/**
 * Created by devos-f on 02/01/17.
 */
public class GMailListenerDaoImpl extends AbstractListener implements GMailListenerDao {
    public GMailListenerDaoImpl(DAOFactory daoFactory){
        super(daoFactory);
        this.createQuery = "INSERT INTO GMailListener " +
            "(userId, type, name, labelName, topicName) " +
            "VALUES (?, ?, ?, ?, ?)";
        this.findQuery = "SELECT l.id AS listenerId, l.type AS listenerType, l.name AS listenerName, " +
                "l.labelName AS listenerLabelName, l.topicName AS listenerTopicName, l.userId AS listenerUserId, " +
                "a.id AS actionId, a.type AS actionType, a.name AS actionName " +
                "FROM GMailListener l " +
                "INNER JOIN ListenerAction la " +
                "ON la.listenerId = l.id " +
                "INNER JOIN Action a " +
                "ON a.id = la.actionId " +
                "WHERE a.userId = ? " +
                "AND a.userId = l.userId " +
                "AND a.id = ?";
        this.findAllQuery = "SELECT id AS listenerId, type AS listenerType, name AS listenerName, " +
                "labelName AS listenerLabelName, topicName AS listenerTopicName, userId as listenerUserId " +
                "FROM GMailListener";
    }

    @Override
    public void create(GMailListener listener, Long userId) throws DAOException {
        super.create(listener, userId, listener.getType(), listener.getName(),
                listener.getLabelName(), listener.getTopicName());
    }

    @Override
    public GMailListener find(Long id, Long userId) throws DAOException {
        return super.find(id, userId, new GMailListener());
    }

    @Override
    public List<GMailListener> findAll() throws DAOException {
        return super.findAll(new GMailListenerFactory());
    }
}
