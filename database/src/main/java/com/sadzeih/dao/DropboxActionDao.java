package com.sadzeih.dao;

import com.sadzeih.beans.DropboxAction;
import com.sadzeih.beans.DropboxListener;
import com.sadzeih.beans.GMailAction;
import com.sadzeih.daoHelper.DAOException;

import java.util.List;

public interface DropboxActionDao {
    void create(DropboxAction action, Long userId) throws DAOException;
    DropboxAction find(Long id, Long userId) throws DAOException;
    void remove(Long id, Long userId) throws DAOException;
}