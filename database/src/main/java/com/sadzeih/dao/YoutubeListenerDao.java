package com.sadzeih.dao;

import com.sadzeih.beans.GMailListener;
import com.sadzeih.beans.YoutubeListener;
import com.sadzeih.daoHelper.DAOException;

import java.util.List;

public interface YoutubeListenerDao {
    // This function do not add links because they are already added when creating the listener
    void create(YoutubeListener action, Long userId) throws DAOException;
    YoutubeListener find(Long id, Long userId) throws DAOException;
    List<YoutubeListener> findAll() throws DAOException;
    void remove(Long id, Long userId) throws DAOException;
}
