package com.sadzeih.dao;

import com.sadzeih.beans.YoutubeAction;
import com.sadzeih.daoHelper.DAOException;

public interface YoutubeActionDao {
    void create(YoutubeAction action, Long userId) throws DAOException;
    YoutubeAction find(Long id, Long userId) throws DAOException;
    void remove(Long id, Long userId) throws DAOException;
}
