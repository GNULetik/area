package com.sadzeih.dao;

import com.sadzeih.beans.GMailListener;
import com.sadzeih.beans.GMailListenerFactory;
import com.sadzeih.beans.YoutubeListener;
import com.sadzeih.beans.YoutubeListenerFactory;
import com.sadzeih.daoHelper.DAOException;
import com.sadzeih.daoHelper.DAOFactory;

import java.util.List;

/**
 * Created by devos-f on 02/01/17.
 */
public class YoutubeListenerDaoImpl extends AbstractListener implements YoutubeListenerDao {
    public YoutubeListenerDaoImpl(DAOFactory daoFactory){
        super(daoFactory);
        this.createQuery = "INSERT INTO YoutubeListener " +
            "(userId, type, name) " +
            "VALUES (?, ?, ?)";
        this.findQuery = "SELECT l.id AS listenerId, l.type AS listenerType, l.name AS listenerName, " +
                "l.userId AS listenerUserId, " +
                "a.id AS actionId, a.type AS actionType, a.name AS actionName " +
                "FROM YoutubeListener l " +
                "INNER JOIN ListenerAction la " +
                "ON la.listenerId = l.id " +
                "INNER JOIN Action a " +
                "ON a.id = la.actionId " +
                "WHERE a.userId = ? " +
                "AND a.userId = l.userId " +
                "AND l.id = ?";
        this.findAllQuery = "SELECT id AS listenerId, type AS listenerType, name AS listenerName, " +
                "userId as listenerUserId " +
                "FROM YoutubeListener";
    }

    @Override
    public void create(YoutubeListener listener, Long userId) throws DAOException {
        super.create(listener, userId, listener.getType(), listener.getName());
    }

    @Override
    public YoutubeListener find(Long id, Long userId) throws DAOException {
        return super.find(id, userId, new YoutubeListener());
    }

    @Override
    public List<YoutubeListener> findAll() throws DAOException {
        return super.findAll(new YoutubeListenerFactory());
    }
}
