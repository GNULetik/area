package com.sadzeih.dao;

import com.sadzeih.beans.DropboxListener;
import com.sadzeih.beans.DropboxListenerFactory;
import com.sadzeih.beans.GMailListener;
import com.sadzeih.beans.GMailListenerFactory;
import com.sadzeih.daoHelper.DAOException;
import com.sadzeih.daoHelper.DAOFactory;

import java.util.List;

/**
 * Created by devos-f on 02/01/17.
 */
public class DropboxListenerDaoImpl extends AbstractListener implements DropboxListenerDao {
    public DropboxListenerDaoImpl(DAOFactory daoFactory){
        super(daoFactory);
        this.createQuery = "INSERT INTO DropboxListener " +
            "(userId, type, name, labelName, topicName) " +
            "VALUES (?, ?, ?, ?)";
        this.findQuery = "SELECT l.id AS listenerId, l.type AS listenerType, l.name AS listenerName, " +
                "l.userId AS listenerUserId, " +
                "a.id AS actionId, a.type AS actionType, a.name AS actionName " +
                "FROM DropboxListener l " +
                "INNER JOIN ListenerAction la " +
                "ON la.listenerId = l.id " +
                "INNER JOIN Action a " +
                "ON a.id = la.actionId " +
                "WHERE a.userId = ? " +
                "AND a.userId = l.userId " +
                "AND a.id = ?";
        this.findAllQuery = "SELECT id AS listenerId, type AS listenerType, name AS listenerName, " +
                "userId as listenerUserId " +
                "FROM DropboxListener";
        this.findByUserIdQuery = "SELECT id AS listenerId, type AS listenerType, name AS listenerName, " +
                "userId as listenerUserId " +
                "FROM DropboxListener " +
                "WHERE userId = ?";
    }

    @Override
    public void create(DropboxListener listener, Long userId) throws DAOException {
        super.create(listener, userId, listener.getType(), listener.getName());
    }

    @Override
    public DropboxListener find(Long id, Long userId) throws DAOException {
        return super.find(id, userId, new DropboxListener());
    }

    @Override
    public List<DropboxListener> findByUserId(Long userId) throws DAOException {
        return super.findByUserId(new DropboxListenerFactory(), userId);
    }
}
