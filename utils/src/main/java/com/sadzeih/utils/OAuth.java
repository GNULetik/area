package com.sadzeih.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.social.oauth2.OAuth2Template;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

/**
 * Created by sadzeih on 15/12/16.
 */
public abstract class OAuth {

    protected String clientId;
    protected String secretId;
    protected String authorizeURL;
    protected String accessTokenURL;
    protected String redirectURI;
    protected String code;
    protected String scope;

    protected String token;

/*    public OAuth(String clientId, String secretId, String authorizeURL, String redirectURI, String accessTokenURL)
    {
        this.clientId = clientId;
        this.secretId = secretId;
        this.authorizeURL = authorizeURL;
        this.redirectURI = redirectURI;
        this.accessTokenURL = accessTokenURL;
        this.restTemplate = new RestTemplate();
    }
*/
    public void setCode(String code)
    {
        this.code = code;
    }

    public void setRedirectURI(String redirectURI)
    {
        this.redirectURI = redirectURI;
    }

    protected RestTemplate restTemplate()
    {
        return new RestTemplate();
    }

    public String buildAuthorizeURL(String state)
    {
        String url = authorizeURL;
        url += "?client_id=" + clientId;
        if (scope != null && !scope.equals("")) {
            url += "&scope=" + scope;
            url += "&access_type=offline";
        }
        url += "&response_type=code";
        url += "&redirect_uri=" + redirectURI;
        url += "&state=" + state;
        return url;
    }

    public void postCode()
    {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
        params.add("grant_type", "authorization_code");
        params.add("code", this.code);
        params.add("redirect_uri", redirectURI);
        params.add("client_id", clientId);
        params.add("client_secret", secretId);
        RestTemplate restTemplate = restTemplate();
        setToken(restTemplate.postForObject(accessTokenURL, params, Map.class));
    }

    public void setToken(Map<String, Object> result)
    {
         this.token = (String)result.get("access_token");
    }

    public String getToken()
    {
        return this.token;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setSecretId(String secretId) {
        this.secretId = secretId;
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getSecretId()
    {
        return this.secretId;
    }

    public void setAuthorizeURL(String authorizeURL) {
        this.authorizeURL = authorizeURL;
    }

    public void setAccessTokenURL(String accessTokenURL) {
        this.accessTokenURL = accessTokenURL;
    }
}
