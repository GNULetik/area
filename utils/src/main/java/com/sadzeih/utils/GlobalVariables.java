package com.sadzeih.utils;

/**
 * Created by desrum_m on 02/02/17.
 */
public class GlobalVariables {

    private static String getEnvVar() {
        return System.getenv("SPRING_DEV");
    }

    public static String getFrontUrl() {
        String env = getEnvVar();
        if (env == null) {
            return "https://area.sadzeih.com";
        } else {
            return "http://localhost:3000";
        }
    }

    public static String getAPIUrl() {
        String env = getEnvVar();
        if (env == null) {
            return "https://area-api.sadzeih.com";
        } else {
            return "http://localhost:3333";
        }
    }
}
