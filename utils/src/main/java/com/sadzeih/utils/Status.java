package com.sadzeih.utils;

/**
 * Created by devos-f on 03/01/17.
 */
public class Status {

    private String type;
    private String message;
    private Object object;

    public Status()
    {

    }

    public Status(String type, String message, Object object)
    {
        this.type = type;
        this.message = message;
        this.object = object;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
}
