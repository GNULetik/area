import {Component} from "@angular/core";
/**
 * Created by descam_d on 14/01/17.
 */

@Component({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl: 'app.component.html'
})

export class AppComponent {
  title = 'Area';
}
