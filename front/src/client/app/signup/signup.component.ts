import {Component} from "@angular/core";
import {AuthenticationService} from "../authentication/authentication.service";
import {Router} from "@angular/router";
import {User} from "../authentication/user";
/**
 * Created by descam_d on 31/01/17.
 */

@Component({
    moduleId: module.id,
    selector: 'signup-form',
    providers: [AuthenticationService],
    templateUrl: 'signup.component.html'
})

export class SignUpComponent {
    public user = new User("", "");
    public errorMsg = '';

    constructor(private authService: AuthenticationService,
                private route: Router) {

    }

    signup() {
        this.authService.register(this.user).subscribe((result) => {
            if (result.type === "error") {
                this.errorMsg = result.message;
            } else {
                this.route.navigate(['/login']);
            }
        });
    }
}
