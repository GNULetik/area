import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {SignUpComponent} from "./signup/signup.component";
import {AuthenticationComponent} from "./authentication/authentication.component";
import {PrivateComponent} from "./authentication/private.component";

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: '', redirectTo: '/home', pathMatch: 'full' },
      { path: 'home', component: PrivateComponent },
      { path: 'login', component: AuthenticationComponent },
      { path: 'signup', component: SignUpComponent}
      /* define app module routes here, e.g., to lazily load a module
         (do not place feature module routes here, use an own -routing.module.ts in the feature instead)
       */
    ])
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

