import {Component} from "@angular/core";
import {ActionService} from "./action.service";
import {Action} from "./action";
/**
 * Created by descam_d on 05/02/17.
 */

@Component({
    moduleId: module.id,
    selector: 'action',
    providers: [ActionService],
    templateUrl: 'action.component.html'
})

export class ActionComponent {
    listenerList: Action[];

    constructor(private service : ActionService) {

    }

    ngOnInit() {
        this.service.getListeners().subscribe(res => {
            this.listenerList = res.object as Action[];
        });
    }

    delete(id: number) {
        this.service.delete(id).subscribe(res => {
            console.log(res);
        });
    }

    deleteLink(id: number) {
        this.service.deleteLink(id).subscribe(res => {
            console.log(res);
        });
    }
}
