/**
 * Created by descam_d on 05/02/17.
 */

export class Action {
    id: number;
    userId: number;
    type: string;
    name: string;
    links: Action[];
}