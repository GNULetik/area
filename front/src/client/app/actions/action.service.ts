import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {User} from "../authentication/user";
import {Env} from "../env";
import {GmailAction, GmailListener, DropboxAction, YoutubeAction} from "../authentication/AddAction";
/**
 * Created by descam_d on 05/02/17.
 */

@Injectable()
export class  ActionService {
    private API_URL = Env.getAPIUrl();

    constructor(
        private http: Http
    ) {}

    getListeners() {
        let token = User.getUser().token;
        let formData = new FormData();
        formData.append("token", token);

        return this.http.post(this.API_URL + "/listener/getAll", formData).map(res => res.json());
    }

    getActions() {
        let token = User.getUser().token;
        let formData = new FormData();
        formData.append("token", token);

        return this.http.post(this.API_URL + "/action/getAll", formData).map(res => res.json());
    }


    delete(id: number) {
        let token = User.getUser().token;
        let formData = new FormData();

        formData.append("id", id.toString());
        formData.append("token", token);
        return this.http.post(this.API_URL + "/listener/delete", formData).map(res => res.json());
    }

    deleteLink(id: number) {
        let token = User.getUser().token;
        let formData = new FormData();

        formData.append("actionId", id.toString());
        formData.append("token", token);
        return this.http.post(this.API_URL + "/listener/deleteLink", formData).map(res => res.json());
    }

    addLink(listenerId: number, actionId: number) {
        let token = User.getUser().token;
        let formData = new FormData();

        console.log("Listener Id :", listenerId, "action Id", actionId);
        formData.append("token", token);
        formData.append("listenerId", listenerId);
        formData.append("actionId", actionId);
        return this.http.post(this.API_URL + "/listener/link", formData).map(res => res.json());
    }

    createGMailAction(action: GmailAction) {
        let token = User.getUser().token;
        let formData = new FormData();

        formData.append("token", token);
        formData.append("name", action.name);
        formData.append("body", action.body);
        formData.append("subject", action.subject);
        formData.append("destinationAddress", action.destination);
        return this.http.post(this.API_URL + "/gmail/createAction", formData).map(res => res.json());
    }

    createGMailListener(listener: GmailListener) {
        let token = User.getUser().token;
        let formData = new FormData();

        formData.append("token", token);
        formData.append("name", listener.name);
        formData.append("topicName", listener.labelName);
        formData.append("topicLabel", listener.topicName);
        return this.http.post(this.API_URL + "/gmail/createListener", formData).map(res => res.json());
    }

    createDropboxAction(action: DropboxAction) {
        let token = User.getUser().token;
        let formData = new FormData();

        formData.append("token", token);
        formData.append("name", action.name);
        return this.http.post(this.API_URL + "/dropbox/createAction", formData).map(res => res.json());
    }

    createDropboxListener(listener: DropboxAction) {
        let token = User.getUser().token;
        let formData = new FormData();

        formData.append("token", token);
        formData.append("name", listener.name);
        return this.http.post(this.API_URL + "/dropbox/createListener", formData).map(res => res.json());
    }

    createYoutubeAction(action: YoutubeAction) {
        let token = User.getUser().token;
        let formData = new FormData();

        formData.append("token", token);
        formData.append("name", action.name);
        formData.append("video", action.idVideo);
        return this.http.post(this.API_URL + "/youtube/createAction", formData).map(res => res.json());
    }

    createYoutubeListener(listener: YoutubeAction) {
        let token = User.getUser().token;
        let formData = new FormData();

        formData.append("token", token);
        formData.append("name", listener.name);
        return this.http.post(this.API_URL + "/youtube/createListener", formData).map(res => res.json());
    }
}
