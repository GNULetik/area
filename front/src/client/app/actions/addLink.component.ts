import {ActionService} from "./action.service";
import {Component, Input} from "@angular/core";
import {Action} from "./action";
/**
 * Created by descam_d on 06/02/17.
 */

@Component({
    moduleId: module.id,
    selector: 'addLink',
    providers: [ActionService],
    templateUrl: 'addLink.component.html'
})

export class AddLinkComponent {
    linksList: Action[];

    @Input()
    parentAction: Action;

    constructor(private service : ActionService) {

    }

    ngOnInit() {
        this.service.getActions().subscribe(res => {
           this.linksList = res.object as Action[];
        });
    }

    add(linkId: number) {
        console.log(this.parentAction);
        console.log("toto");
        this.service.addLink(this.parentAction.id, linkId).subscribe(res => {
            console.log(res);
        });
    }
}
