/**
 * Created by descam_d on 14/01/17.
 */

import {Component, OnInit} from "@angular/core";
import {AuthenticationService} from "./authentication.service";

@Component({
    moduleId: module.id,
    selector: 'private',
    providers: [AuthenticationService],
    templateUrl: 'private.component.html'
})

export class PrivateComponent implements OnInit {

    constructor(
        private authService: AuthenticationService
    ) {}

    ngOnInit() {
        this.authService.checkCredentials();
    }

    logout() {
        this.authService.logout();
    }

    gmail() {
        this.authService.service("gmail").subscribe(res => {
            console.log(res);
            window.location.href=res.object;
        });
    }

    twitter() {
        this.authService.service("twitter").subscribe(res => {
            console.log(res);
            window.location.href=res.object;
        });
    }

    spotify() {
        this.authService.service("spotify").subscribe(res => {
            console.log(res);
            window.location.href=res.object;
        });
    }

    youtube() {
        this.authService.service("youtube").subscribe(res => {
            console.log(res);
            window.location.href=res.object;
        });
    }

    dropbox() {
        this.authService.service("dropbox").subscribe(res => {
            console.log(res);
            window.location.href=res.object;
        });
    }
}
