import {Component} from "@angular/core";
import {AuthenticationService} from "./authentication.service";
import {ActionService} from "../actions/action.service";
import {YoutubeAction} from "./AddAction";
/**
 * Created by descam_d on 08/02/17.
 */

@Component({
    moduleId: module.id,
    selector: 'youtube',
    providers: [AuthenticationService, ActionService],
    templateUrl: 'youtube.component.html'
})


export class YoutubeComponent {
    Action: YoutubeAction = new YoutubeAction();
    Listener: YoutubeAction = new YoutubeAction();

    constructor(private authService: AuthenticationService,
                private service: ActionService) {

    }

    auth() {
        this.authService.service("youtube").subscribe(res => {
            window.location.href=res.object;
        });
    }

    addAction() {
        console.log(this.Action);
        this.service.createYoutubeAction(this.Action).subscribe(res => {
            console.log(res);
        });
    }

    addListener() {
        console.log(this.Listener);
        this.service.createYoutubeListener(this.Listener).subscribe(res => {
            console.log(res);
        });
    }

}