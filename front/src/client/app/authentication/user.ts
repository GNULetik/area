/**
 * Created by descam_d on 02/02/17.
 */

export class User {
    email: string;
    password: string;
    id: number;
    token: string;

    constructor(email: string, password: string) {
        this.email = email;
        this.password = password;
    }

    public store(): void {
        localStorage.setItem("user", JSON.stringify(this));
    }

    public static getUser(): User {
        return JSON.parse(localStorage.getItem("user"));
    }
}