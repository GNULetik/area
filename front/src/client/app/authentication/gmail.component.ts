import {AuthenticationService} from "./authentication.service";
import {Component} from "@angular/core";
import {GmailAction, GmailListener} from "./AddAction";
import {ActionService} from "../actions/action.service";
/**
 * Created by descam_d on 07/02/17.
 */

@Component({
    moduleId: module.id,
    selector: 'gmail',
    providers: [AuthenticationService, ActionService],
    templateUrl: 'gmail.component.html'
})

export class GMailComponent {
    GmailAction: GmailAction = new GmailAction();
    GmailListener: GmailListener = new GmailListener();

    constructor(private authService: AuthenticationService,
    private service: ActionService) {

    }

    auth() {
        this.authService.service("gmail").subscribe(res => {
            window.location.href=res.object;
        });
    }

    addAction() {
        console.log(this.GmailAction);
        this.service.createGMailAction(this.GmailAction).subscribe(res => {
            console.log(res);
        });
    }

    addListener() {
        console.log(this.GmailListener);
        this.service.createGMailListener(this.GmailListener).subscribe(res => {
            console.log(res);
        });
    }

}
