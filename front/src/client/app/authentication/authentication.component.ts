/**
 * Created by descam_d on 14/01/17.
 */

import {Component} from "@angular/core";
import {AuthenticationService} from "./authentication.service";
import {Router} from "@angular/router";
import {User} from "./user";

@Component({
  moduleId: module.id,
  selector: 'login-form',
  providers: [AuthenticationService],
  templateUrl: 'authentication.component.html'
})

export class AuthenticationComponent {
  public user = new User("", "");
  public errorMsg = '';

  constructor(private authService: AuthenticationService,
              private route: Router) {

  }

  login() {
    this.authService.login(this.user).subscribe((result) => {
      if (result) {
        this.route.navigate(['/home']);
      } else {
        this.errorMsg = "Failed to login";
      }
    });
  }

  signup() {
    this.route.navigate(['/signup']);
  }
}
