import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {Http, URLSearchParams} from "@angular/http";
import {User} from "./user";
import 'rxjs/Rx';
import {Env} from "../env";

/**
 * Created by descam_d on 14/01/17.
 */

@Injectable()
export class  AuthenticationService {

    private API_URL = Env.getAPIUrl();

    constructor(
        private route: Router,
        private http: Http
    ) {}

    logout() {
        let params = new URLSearchParams();
        params.set('id', User.getUser().id.toString());

        this.http.get(this.API_URL + "/account/disconnect/" + User.getUser().id.toString()).map(res => res.json()).subscribe(res => {
            console.log(res);
            localStorage.removeItem("user");
            this.route.navigate(['/login']);
        });
    }

    login(user: User) {
        let formData = new FormData();

        formData.append("email", user.email);
        formData.append("hashpassword", user.password);


        return this.http.post(this.API_URL + "/account/connect", formData).map(res => res.json()).map((res) => {
            if (res.type === "success") {
                user.id = res.object.userId;
                user.token = res.object.token;
                user.store();
                return true;
            }

            return false;
        });
    }

    checkCredentials() {
        if (localStorage.getItem("user") === null) {
            this.route.navigate(['/login']);
        }
    }

    register(user: User) {
        let formData = new FormData();

        formData.append("email", user.email);
        formData.append("hashpassword", user.password);

        return this.http.post(this.API_URL + "/account/register", formData).map(res => res.json());
    }

    service(service: string) {
        let token = User.getUser().token;
        let formData = new FormData();
        formData.append("token", token);
        console.log(token);
        return this.http.post(this.API_URL + "/" + service + "/auth", formData).map(res => res.json());
    }
}

