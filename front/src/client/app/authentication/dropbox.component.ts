import {Component} from "@angular/core";
import {AuthenticationService} from "./authentication.service";
import {ActionService} from "../actions/action.service";
import {DropboxAction} from "./AddAction";
/**
 * Created by descam_d on 08/02/17.
 */

@Component({
    moduleId: module.id,
    selector: 'dropbox',
    providers: [AuthenticationService, ActionService],
    templateUrl: 'dropbox.component.html'
})

export class DropboxComponent {
    Action: DropboxAction = new DropboxAction();
    Listener: DropboxAction = new DropboxAction();

    constructor(private authService: AuthenticationService,
                private service: ActionService) {

    }

    auth() {
        this.authService.service("dropbox").subscribe(res => {
            window.location.href=res.object;
        });
    }

    addAction() {
        console.log(this.Action);
        this.service.createDropboxAction(this.Action).subscribe(res => {
            console.log(res);
        });
    }

    addListener() {
        console.log(this.Listener);
        this.service.createDropboxListener(this.Listener).subscribe(res => {
            console.log(res);
        });
    }

}
