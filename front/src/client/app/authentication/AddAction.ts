/**
 * Created by descam_d on 08/02/17.
 */


export class GmailAction {
    name: string;
    body: string;
    subject: string;
    destination: string;

    constructor() {

    }
}

export class GmailListener {
    name: string;
    topicName: string;
    labelName: string;
}

export class DropboxAction {
    name: string;
}

export class YoutubeAction {
    name: string;
    idVideo: number;
}