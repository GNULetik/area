import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import {HttpModule} from "@angular/http";
import {PrivateComponent} from "./authentication/private.component";
import {AppComponent} from "./app.component";
import {SignUpComponent} from "./signup/signup.component";
import {AuthenticationComponent} from "./authentication/authentication.component";
import {AppRoutingModule} from "./app-routing.module";
import {ActionComponent} from "./actions/action.component";
import {AddLinkComponent} from "./actions/addLink.component";
import {GMailComponent} from "./authentication/gmail.component";
import {DropboxAction} from "./authentication/AddAction";
import {DropboxComponent} from "./authentication/dropbox.component";
import {YoutubeComponent} from "./authentication/youtube.component";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule
  ],
  declarations: [
    AppComponent,
    AuthenticationComponent,
    PrivateComponent,
    SignUpComponent,
    ActionComponent,
    AddLinkComponent,
    GMailComponent,
      DropboxComponent,
      YoutubeComponent
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
